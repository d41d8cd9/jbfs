package com.example.jbfs;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.ThreadSafe;

import com.example.jbfs.exceptions.DirectoryIsNotEmpty;
import com.example.jbfs.exceptions.FileOrDirectoryExists;
import com.example.jbfs.exceptions.NoSuchFileOrDirectory;
import com.example.jbfs.exceptions.NotADirectory;
import com.example.jbfs.exceptions.NotAFile;
import com.google.common.base.Preconditions;

/**
 * Implementation of {@link FileSystem} that uses real file from real file system as a storage backend.
 * <p>
 * Binary interface:
 * <p>
 * <table>
 * <tr><th>Byte positions</th><th>Purpose</th></tr>
 * <tr><td>[0, 1)</td><td>Version of the format</td></tr>
 * <tr><td>[1, 4)</td><td>Unused</td></tr>
 * <tr><td>[4, 8)</td><td>Block size</td></tr>
 * <tr><td>[8, 16)</td><td>Size of allocator's bit set</td></tr>
 * <tr><td>[16, 24)</td><td>Offset to the first block that always stores content ot the root directory</td></tr>
 * <tr><td>[24, 24 + bit set node size)</td><td>Bit set</td></tr>
 * </table>
 * <p>
 * Blocks are written after the bit set. Position of the first block is aligned to the block size.
 */
@ParametersAreNonnullByDefault
@ThreadSafe
public class FileBackedFileSystem implements AutoCloseable, FileSystem {
    private static final byte NEW_VERSION = 2;
    /** Separator between directories in path to a file/directory. */
    private static final String SEPARATOR = "/";
    private final HoldingAllocator allocatorHolder;
    private final FileChannelAdaptor fileChannel;
    private final BlockGuardFactory blockGuardFactory;
    private final long rootNodeOffset;

    private FileBackedFileSystem(FileChannelAdaptor fileChannel, Allocator allocator,
            LockBasedBlockGuardFactory blockGuardFactory, long rootNodeOffset)
    {
        this.allocatorHolder = new HoldingAllocator(allocator);
        this.blockGuardFactory = blockGuardFactory;
        this.rootNodeOffset = rootNodeOffset;
        this.fileChannel = fileChannel;
    }

    /**
     * Resizes real file and creates new file system in the real file. Requires that real file to be already created
     * before calling this method.
     *
     * @param path      Path to real file.
     * @param size      Backend file size in bytes.
     * @param blockSize Size of each block.
     * @throws IOException Re-thrown from operations with real file.
     */
    public static void format(Path path, long size, int blockSize) throws IOException {
        Preconditions.checkArgument(size % blockSize == 0);
        try (FileChannel fileChannel = FileChannel.open(path,
                StandardOpenOption.READ, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING))
        {
            new FileChannelAdaptor(fileChannel).resize(size);
            FileBackedFileSystem.format(fileChannel, blockSize);
        }
    }

    /**
     * Mount a file system.
     *
     * @param path Real file that stores content of a file system.
     * @return The file system.
     * @throws IOException Re-thrown from operations with real file.
     */
    public static FileBackedFileSystem mount(Path path) throws IOException {
        // this file descriptor should not be closed
        @SuppressWarnings("squid:S2095")
        FileChannel fileChannel = FileChannel.open(path, StandardOpenOption.READ, StandardOpenOption.WRITE);
        FileChannelAdaptor fileChannelAdaptor = new FileChannelAdaptor(fileChannel);
        fileChannel.position(0);
        ByteBuffer storage = ByteBuffer.allocate(24);
        fileChannel.read(storage);
        storage.flip();
        byte storageVersion = storage.get();
        // 3 bytes reserved, other ints padded
        storage.get();
        storage.get();
        storage.get();
        Allocator allocator;
        int blockSize;
        long rootNodeOffset;
        Preconditions.checkState(storageVersion == NEW_VERSION, "Unsupported version %s", storageVersion);
        long fileSystemSize = fileChannel.size();
        blockSize = storage.getInt();
        long firstBlockOffset = storage.getLong();
        rootNodeOffset = firstBlockOffset + blockSize;
        long allocatorHeader = storage.position();
        allocator = new ChunkListBitSetAllocator(
                fileChannelAdaptor, allocatorHeader, firstBlockOffset, blockSize, fileSystemSize);
        return new FileBackedFileSystem(
                fileChannelAdaptor,
                allocator,
                new LockBasedBlockGuardFactory(),
                rootNodeOffset);
    }

    /**
     * See {@link #format(Path, long, int)}
     *
     * @param fileChannel File opened with read+write access.
     * @param blockSize   Size of each block.
     */
    public static void format(FileChannel fileChannel, int blockSize) {
        // 4 bytes for version
        // 4 bytes for block size
        // 8 bytes for first block offset
        // 16 bytes bitset allocator header
        // 32 bytes total
        long firstBlockStart = (long) blockSize * ((blockSize + 32 - 1) / blockSize);
        try {
            FileChannelAdaptor adaptor = new FileChannelAdaptor(fileChannel);
            ByteBuffer header = ByteBuffer.allocate(16);
            header.position(0);
            header.put(NEW_VERSION);
            header.put((byte) 0);
            header.put((byte) 0);
            header.put((byte) 0);
            header.putInt(blockSize);
            header.putLong(firstBlockStart);
            long allocatorHeaderOffset = header.position();
            header.flip();
            fileChannel.write(header, 0);
            ChunkListBitSetAllocator.format(adaptor, allocatorHeaderOffset, firstBlockStart, blockSize);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * Closes real file.
     *
     * @throws IOException If file can't be closed.
     */
    @Override
    public void close() throws IOException {
        fileChannel.close();
    }

    @Override
    public List<DirectoryChildInfo> listDirectory(String path) throws InterruptedException {
        try (Guarded<Node> node = findNode(path, BlockGuardFactory.Kind.SHARED)) {
            if ((node.getObject() instanceof DirectoryNode)) {
                return ((DirectoryNode) node.getObject()).list();
            } else {
                throw new NotADirectory(path);
            }
        }
    }

    @Override
    public void makeDirectory(String path) throws InterruptedException {
        List<String> names = splitPath(path);
        Preconditions.checkArgument(!path.isEmpty());
        String lastName = names.remove(names.size() - 1);
        try (Guarded<Node> parentDirNode = findNode(names, BlockGuardFactory.Kind.EXCLUSIVE);
             HoldingAllocator.Holder allocator = allocatorHolder.get())
        {
            if (parentDirNode.getObject() instanceof DirectoryNode) {
                long blockForDirectory = allocator.allocate();
                ChunkListStorage.writeEmptyChunkHeader(fileChannel, blockForDirectory);
                try {
                    if (!((DirectoryNode) parentDirNode.getObject()).addDirectory(lastName, blockForDirectory)) {
                        throw new FileOrDirectoryExists(path);
                    }
                } catch (RuntimeException exc) {
                    allocator.deallocate(blockForDirectory);
                    throw exc;
                }
            } else {
                throw new NotADirectory(names);
            }
            allocator.success();
        }
    }

    @Override
    public void deleteDirectory(String path) throws InterruptedException {
        List<String> names = splitPath(path);
        requireNonEmptyPath(names);
        String dstDirName = names.remove(names.size() - 1);
        try (Guarded<Node> parentDirNode = findNode(names, BlockGuardFactory.Kind.EXCLUSIVE);
             HoldingAllocator.Holder allocator = allocatorHolder.get())
        {
            if (!(parentDirNode.getObject() instanceof DirectoryNode)) {
                throw new NotADirectory(names);
            }
            DirectoryChildInfo dstDirNodeInfo = ((DirectoryNode) parentDirNode.getObject()).findNode(dstDirName);
            if (dstDirNodeInfo == null) {
                throw new NoSuchFileOrDirectory(path);
            }
            if (!dstDirNodeInfo.isDirectory()) {
                throw new NotADirectory(path);
            }
            long dirContentOffset = dstDirNodeInfo.getContentOffset();
            try (BlockGuard dirGuard = blockGuardFactory.acquire(dirContentOffset, BlockGuardFactory.Kind.EXCLUSIVE)) {
                DirectoryNode dstDirNode = new DirectoryNode(fileChannel, allocator, dirContentOffset);
                if (!dstDirNode.isEmpty()) {
                    throw new DirectoryIsNotEmpty(path);
                }
                ((DirectoryNode) parentDirNode.getObject()).deallocateNode(dstDirNodeInfo);
                dstDirNode.makeChunkList().deallocate();
            }
            allocator.success();
        }
    }

    private void requireNonEmptyPath(List<String> names) {
        Preconditions.checkArgument(!names.isEmpty(), "Empty path");
    }

    @Override
    public void makeFile(String path) throws InterruptedException {
        List<String> names = splitPath(path);
        requireNonEmptyPath(names);
        String fileName = names.remove(names.size() - 1);
        try (Guarded<Node> parentDirNode = findNode(names, BlockGuardFactory.Kind.EXCLUSIVE);
             HoldingAllocator.Holder allocator = allocatorHolder.get())
        {
            if (!(parentDirNode.getObject() instanceof DirectoryNode)) {
                throw new NotADirectory(names);
            }
            long blockForFile = allocator.allocate();
            ChunkListStorage.writeEmptyChunkHeader(fileChannel, blockForFile);
            try {
                if (!((DirectoryNode) parentDirNode.getObject()).addFile(fileName, blockForFile)) {
                    throw new FileOrDirectoryExists(path);
                }
            } catch (RuntimeException exc) {
                allocator.deallocate(blockForFile);
                throw exc;
            }
            allocator.success();
        }
    }

    @Override
    public void deleteFile(String path) throws InterruptedException {
        List<String> names = splitPath(path);
        requireNonEmptyPath(names);
        String fileName = names.remove(names.size() - 1);
        try (Guarded<Node> dirNode = findNode(names, BlockGuardFactory.Kind.EXCLUSIVE);
             HoldingAllocator.Holder allocator = allocatorHolder.get())
        {
            if (!(dirNode.getObject() instanceof DirectoryNode)) {
                throw new NotADirectory(names);
            }
            DirectoryChildInfo fileNodeInfo = ((DirectoryNode) dirNode.getObject()).findNode(fileName);
            if (fileNodeInfo == null) {
                throw new NoSuchFileOrDirectory(path);
            }
            if (!fileNodeInfo.isFile()) {
                throw new NotAFile(path);
            }
            long fileContentOffset = fileNodeInfo.getContentOffset();
            try (BlockGuard fileGuard = blockGuardFactory
                    .acquire(fileContentOffset, BlockGuardFactory.Kind.EXCLUSIVE))
            {
                FileNode fileNode = new FileNode(fileChannel, allocator, fileContentOffset);
                ((DirectoryNode) dirNode.getObject()).deallocateNode(fileNodeInfo);
                fileNode.makeChunkList().deallocate();
            }
            allocator.success();
        }
    }

    @Override
    public InputStream openForReading(String path) throws InterruptedException {
        Guarded<Node> node = findNode(path, BlockGuardFactory.Kind.SHARED);
        if ((node.getObject() instanceof FileNode)) {
            return new JbfsFileInputStream(node.getObject().makeChunkList(), node.getGuard());
        } else {
            try (Guarded<Node> ignored = node) {
                throw new NotAFile(path);
            }
        }
    }

    @Override
    public OutputStream openForWriting(String path) throws InterruptedException {
        Guarded<Node> node = findNode(path, BlockGuardFactory.Kind.EXCLUSIVE);
        if ((node.getObject() instanceof FileNode)) {
            return new JbfsFileOutputStream(node.getObject().makeChunkList(), node.getGuard());
        } else {
            try (Guarded<Node> ignored = node) {
                throw new NotAFile(path);
            }
        }
    }

    @Override
    public OutputStream openForAppending(String path) throws InterruptedException {
        JbfsFileOutputStream stream = (JbfsFileOutputStream) openForWriting(path);
        stream.getChunkList().forwardToEnd();
        return stream;
    }

    @Override
    public void resize(long size) {
        long oldSize = fileChannel.getSize();
        if (size != oldSize) {
            Preconditions.checkArgument(size > oldSize, "Decreasing size of file system is not supported");
            fileChannel.resize(size);
            allocatorHolder.get().setPositionLimit(size);
        }
    }

    /**
     * See {@link #findNode(List, BlockGuardFactory.Kind)}.
     *
     * @param path String representation of path to node.
     */
    private Guarded<Node> findNode(String path, BlockGuardFactory.Kind kind) throws InterruptedException {
        return findNode(splitPath(path), kind);
    }

    /**
     * Splits the path by directory separator and filters out all empty path parts.
     */
    private List<String> splitPath(String path) {
        return Stream.of(path.split(SEPARATOR))
                .filter(s -> s != null && !s.isEmpty())
                .collect(Collectors.toList());
    }

    /**
     * Traverses directories starting from root directory and returns node for a specified path.
     * Returns the node (file or directory) and acquired block guard for the first block that the node owns.
     * <p>
     * While traversing the method takes shared lock for every non-final node and takes specified by <i>kind</i> lock
     * only for the last node.
     *
     * @param names Path, splitted by directory separator.
     * @param kind  Which kind of lock to acquire for accessing the destination block.
     * @return Node and block guard.
     * @throws InterruptedException If waiting for locking of any block is interrupted.
     */
    private Guarded<Node> findNode(List<String> names, BlockGuardFactory.Kind kind) throws InterruptedException {
        Node result;
        try (CloseableList<BlockGuard> blockGuards = new CloseableList<>();
             HoldingAllocator.Holder allocator = allocatorHolder.get())
        {
            int namesRemained = names.size();
            blockGuards.add(blockGuardFactory.acquire(rootNodeOffset,
                    namesRemained > 0 ? BlockGuardFactory.Kind.SHARED : kind));
            result = new DirectoryNode(fileChannel, allocator, rootNodeOffset);
            StringBuilder pathForError = new StringBuilder("/");
            for (String name : names) {
                --namesRemained;
                if (!(result instanceof DirectoryNode)) {
                    throw new NotADirectory(pathForError.toString());
                }
                pathForError.append(name);
                DirectoryChildInfo node = ((DirectoryNode) result).findNode(name);
                if (node == null) {
                    throw new NoSuchFileOrDirectory(pathForError.toString());
                }
                pathForError.append("/");
                long contentOffset = node.getContentOffset();
                blockGuards.add(blockGuardFactory.acquire(contentOffset,
                        namesRemained > 0 ? BlockGuardFactory.Kind.SHARED : kind));
                if (node.isDirectory()) {
                    result = new DirectoryNode(fileChannel, allocator, contentOffset);
                } else {
                    result = new FileNode(fileChannel, allocator, contentOffset);
                }
                blockGuards.removeFirst().close();
            }
            assert blockGuards.size() == 1;
            Guarded<Node> guard = new Guarded<>(blockGuards.remove(), result);
            allocator.success();
            return guard;
        }
    }
}
