package com.example.jbfs;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Allocator manages constantly sized blocks in byte array or memory mapped file.
 */
@ParametersAreNonnullByDefault
public interface Allocator {
    /**
     * Allocate new block. Should never return zero.
     *
     * @return Offset from start of byte array or file where the allocated block starts.
     * @throws com.example.jbfs.exceptions.NoSpaceLeft When no free blocks left.
     */
    long allocate();

    /**
     * Consumes the block back, allowing to allocate the block later.
     *
     * @param offset Offset from start of byte array or file where the block starts.
     */
    void deallocate(long offset);

    /**
     * @return Size of each allocated block.
     */
    int getBlockSize();

    default void setPositionLimit(long positionLimit) {
        throw new UnsupportedOperationException();
    }
}
