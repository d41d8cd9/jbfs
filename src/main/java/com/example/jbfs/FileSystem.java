package com.example.jbfs;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * A file-system that may
 * <ul>
 * <li>Create and delete files.</li>
 * <li>Read and write file contents.</li>
 * <li>Create and delete directories.</li>
 * <li>List directory contents.</li>
 * </ul>
 * <p>
 * Interface made with expectation that implementation will be thread-safe and will use interruptable blocking
 * operation.
 */
@ParametersAreNonnullByDefault
public interface FileSystem {

    /**
     * Lists contents of the specified directory.
     *
     * @param path Path to directory.
     * @return List of special objects that contains information about each node in directory.
     * @throws InterruptedException                              If awaiting for some lock was interrupted.
     * @throws com.example.jbfs.exceptions.NoSuchFileOrDirectory If specified directory or any of its parent directory
     *                                                           does not exist.
     * @throws com.example.jbfs.exceptions.NotADirectory         If trying to get list of a file.
     */
    List<DirectoryChildInfo> listDirectory(String path) throws InterruptedException;

    /**
     * Creates a directory.
     *
     * @param path Path to the new directory.
     * @throws InterruptedException                              If awaiting for some lock was interrupted.
     * @throws com.example.jbfs.exceptions.FileOrDirectoryExists If parent directory already contains node with that
     *                                                           name.
     * @throws com.example.jbfs.exceptions.NameTooLong           If specified name of a directory longer than 255 bytes.
     * @throws com.example.jbfs.exceptions.NoSpaceLeft           If no free space left in a file system.
     * @throws com.example.jbfs.exceptions.NoSuchFileOrDirectory If parent directory does not exist.
     * @throws com.example.jbfs.exceptions.NotADirectory         If trying to create directory inside a file.
     */
    void makeDirectory(String path) throws InterruptedException;

    /**
     * Creates an empty file.
     *
     * @param path Path to the new new.
     * @throws InterruptedException                              If awaiting for some lock was interrupted.
     * @throws com.example.jbfs.exceptions.FileOrDirectoryExists If parent directory already contains node with that
     *                                                           name.
     * @throws com.example.jbfs.exceptions.NameTooLong           If specified name of a file longer than 255 bytes.
     * @throws com.example.jbfs.exceptions.NoSpaceLeft           If no free space left in a file system.
     * @throws com.example.jbfs.exceptions.NoSuchFileOrDirectory If parent directory does not exist.
     * @throws com.example.jbfs.exceptions.NotADirectory         If trying to create directory inside a file.
     */
    void makeFile(String path) throws InterruptedException;

    /**
     * Deletes a directory.
     *
     * @param path Path to the deleting directory.
     * @throws InterruptedException                              If awaiting for some lock was interrupted.
     * @throws com.example.jbfs.exceptions.DirectoryIsNotEmpty   If trying to delete directory that contains at least
     *                                                           node.
     * @throws com.example.jbfs.exceptions.NoSuchFileOrDirectory If deleting directory or its parent directory does not
     *                                                           exist.
     * @throws com.example.jbfs.exceptions.NotADirectory         If trying to delete a file.
     */
    void deleteDirectory(String path) throws InterruptedException;

    /**
     * Deletes a file.
     *
     * @param path Path to the deleting file.
     * @throws InterruptedException                              If awaiting for some lock was interrupted.
     * @throws com.example.jbfs.exceptions.NoSuchFileOrDirectory If deleting file or its parent directory does not
     *                                                           exist.
     * @throws com.example.jbfs.exceptions.NotADirectory         If trying to delete a directory.
     */
    void deleteFile(String path) throws InterruptedException;

    /**
     * Creates {@link InputStream} that used to read contents of a file.
     * <p>
     * Note that returned stream may hold shared lock to specified file and nobody may write to that file until returned
     * stream will be closed.
     *
     * @param path Path to the specified file.
     * @return Input stream.
     * @throws InterruptedException                              If awaiting for some lock was interrupted.
     * @throws com.example.jbfs.exceptions.NoSuchFileOrDirectory If opening file that does not exist or even its parent directory does not exist.
     * @throws com.example.jbfs.exceptions.NotAFile              If trying to open directory for reading.
     */
    InputStream openForReading(String path) throws InterruptedException;

    /**
     * Creates {@link OutputStream} that used to write contents of a file. Any writes to returned stream will rewrite
     * contents from the start.
     * <p>
     * Note that returned stream may hold exclusive lock to specified file and nobody may read or write that file until
     * returned stream will be closed.
     *
     * @param path Path to the specified file.
     * @return Input stream.
     * @throws InterruptedException                              If awaiting for some lock was interrupted.
     * @throws com.example.jbfs.exceptions.NoSuchFileOrDirectory If opening file that does not exist or even its parent directory does not exist.
     * @throws com.example.jbfs.exceptions.NotAFile              If trying to open directory for writing.
     */
    OutputStream openForWriting(String path) throws InterruptedException;

    /**
     * Works exactly like {@link #openForWriting(String)} but any writes to returned stream will be appended to the end
     * of the file.
     */
    OutputStream openForAppending(String path) throws InterruptedException;

    /**
     * Set new size of file system. Supported only increasing size;
     *
     * @param size New size in bytes.
     */
    void resize(long size);
}
