package com.example.jbfs;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Block guard used to limit concurrent access to some block;
 */
@ParametersAreNonnullByDefault
interface BlockGuard extends AutoCloseable {
    /**
     * Frees lock of some block.
     */
    @Override
    void close();
}
