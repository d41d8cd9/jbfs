package com.example.jbfs;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Decorator for some object that guarded by {@link BlockGuard}.
 */
@ParametersAreNonnullByDefault
class Guarded<T> implements AutoCloseable {
    private final BlockGuard guard;
    private final T object;

    Guarded(BlockGuard guard, T object) {
        this.guard = guard;
        this.object = object;
    }

    BlockGuard getGuard() {
        return guard;
    }

    T getObject() {
        return object;
    }

    @Override
    public void close() {
        guard.close();
    }
}
