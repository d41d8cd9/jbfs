package com.example.jbfs;

import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.ThreadSafe;

import com.google.common.collect.MapMaker;

/**
 * Implementation of block guard factory that uses map of {@link ReadWriteLock} and therefore usage of these factory
 * grants thread-safe access to a file system inside one operating system process.
 */
@ParametersAreNonnullByDefault
@ThreadSafe
class LockBasedBlockGuardFactory implements BlockGuardFactory {
    private final Map<Long, ReadWriteLock> readWriteLocks = new MapMaker()
            .weakValues()
            .makeMap();

    @Override
    public BlockGuard acquire(long offset, Kind kind) {
        ReadWriteLock readWriteLock = readWriteLocks.computeIfAbsent(offset, k -> new ReentrantReadWriteLock());
        return new LockBasedBlockGuard(readWriteLock, kind);
    }

    class LockBasedBlockGuard implements BlockGuard {
        @SuppressWarnings("unused")  // for reference counting
        private final ReadWriteLock lockOwner;
        private final Lock lock;

        LockBasedBlockGuard(ReadWriteLock lockOwner, BlockGuardFactory.Kind kind) {
            this.lockOwner = lockOwner;
            switch (kind) {
                case EXCLUSIVE:
                    this.lock = lockOwner.writeLock();
                    break;
                case SHARED:
                    this.lock = lockOwner.readLock();
                    break;
                default:
                    throw new IllegalArgumentException("Can't handle " + kind);
            }
            this.lock.lock();
        }

        @Override
        public void close() {
            lock.unlock();
        }
    }
}
