package com.example.jbfs;

import java.io.Closeable;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.ThreadSafe;

/**
 * Guard for allocator that deallocates every internally allocated block in case of exception.
 */
@ParametersAreNonnullByDefault
@ThreadSafe
public class HoldingAllocator {
    private final Allocator realAllocator;
    private final Object monitor = new Object();

    HoldingAllocator(Allocator realAllocator) {
        this.realAllocator = realAllocator;
    }

    /**
     * @return New allocator {@link Holder holder}.
     */
    public Holder get() {
        return new Holder();
    }

    /**
     * Closeable allocator that forwards all (de)allocates to the real allocator.
     *
     * Before closing this holder must be called method {@link #success()}, otherwise all allocated blocks will be
     * deallocated back.
     */
    public class Holder implements Allocator, Closeable {
        private final Set<Long> allocates = new HashSet<>();
        private boolean success = false;

        private Holder() {
        }

        @Override
        public long allocate() {
            synchronized (monitor) {
                long result = realAllocator.allocate();
                allocates.add(result);
                return result;
            }
        }

        @Override
        public void deallocate(long offset) {
            synchronized (monitor) {
                allocates.remove(offset);
                realAllocator.deallocate(offset);
            }
        }

        @Override
        public int getBlockSize() {
            return realAllocator.getBlockSize();
        }

        @Override
        public void setPositionLimit(long positionLimit) {
            synchronized (monitor) {
                realAllocator.setPositionLimit(positionLimit);
            }
        }

        /**
         * This method must be called after success of procedure before closing holder.
         * Otherwise all allocated blocks will be deallocated back.
         */
        public void success() {
            success = true;
        }

        @Override
        public void close() {
            if (!success) {
                synchronized (monitor) {
                    allocates.forEach(realAllocator::deallocate);
                }
            }
        }
    }
}
