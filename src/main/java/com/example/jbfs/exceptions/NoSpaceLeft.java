package com.example.jbfs.exceptions;

/**
 * Throws when can't create new file or directory, when can't append data to file.
 */
public class NoSpaceLeft extends JbfsException {
    public NoSpaceLeft() {
        super("No space left");
    }
}
