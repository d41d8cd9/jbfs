package com.example.jbfs.exceptions;

import javax.annotation.ParametersAreNonnullByDefault;


/**
 * Base class for all exceptions related to file system.
 */
@ParametersAreNonnullByDefault
public abstract class JbfsException extends RuntimeException {
    JbfsException(String message) {
        super(message);
    }
}
