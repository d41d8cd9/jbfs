package com.example.jbfs.exceptions;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Throws when file or directory name is longer than 255 bytes.
 */
@ParametersAreNonnullByDefault
public class NameTooLong extends JbfsException {
    public NameTooLong(int length) {
        super("File name too long (" + length + " bytes of 255 allowed)");
    }
}
