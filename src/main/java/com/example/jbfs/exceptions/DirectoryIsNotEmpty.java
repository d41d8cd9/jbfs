package com.example.jbfs.exceptions;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Throws when trying to delete non-empty directory.
 */
@ParametersAreNonnullByDefault
public class DirectoryIsNotEmpty extends JbfsException {
    public DirectoryIsNotEmpty(String path) {
        super("Directory is not empty: " + path);
    }
}
