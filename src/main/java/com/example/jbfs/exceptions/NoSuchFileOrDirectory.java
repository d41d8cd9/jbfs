package com.example.jbfs.exceptions;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Throws when trying to access entity by path that does not exist or trying to create file or directory in non-existent
 * directory.
 */
@ParametersAreNonnullByDefault
public class NoSuchFileOrDirectory extends JbfsException {
    private final String path;

    public NoSuchFileOrDirectory(String path) {
        super("No such file or directory: " + path);
        this.path = path;
    }

    /** Path to non-existent file or directory. */
    public String getPath() {
        return path;
    }
}
