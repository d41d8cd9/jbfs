package com.example.jbfs.exceptions;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Throws when trying to use directory as a file.
 */
@ParametersAreNonnullByDefault
public class NotAFile extends JbfsException {
    public NotAFile(String path) {
        super("Not a file: " + path);
    }
}
