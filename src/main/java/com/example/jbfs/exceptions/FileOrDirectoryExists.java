package com.example.jbfs.exceptions;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Throws when trying to create file or directory at path occupied by another file or directory.
 */
@ParametersAreNonnullByDefault
public class FileOrDirectoryExists extends JbfsException {
    public FileOrDirectoryExists(String path) {
        super("File or directory " + path + " already exists");
    }
}
