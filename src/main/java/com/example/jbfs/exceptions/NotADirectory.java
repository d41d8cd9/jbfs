package com.example.jbfs.exceptions;

import java.util.List;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Throws when trying to use file as a directory.
 */
@ParametersAreNonnullByDefault
public class NotADirectory extends JbfsException {
    public NotADirectory(String path) {
        super("Not a directory: " + path);
    }

    public NotADirectory(List<String> names) {
        this("/" + String.join("/", names));
    }
}
