package com.example.jbfs;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.NotThreadSafe;

import com.example.jbfs.exceptions.NameTooLong;
import com.google.common.base.Charsets;
import com.google.common.primitives.UnsignedBytes;

/**
 * Represents information about directory.
 * <p>
 * Binary interface:
 * <p>
 * <table>
 * <tr><th>Byte positions</th><th>Purpose</th></tr>
 * <tr><td>[0, 1)</td><td>Type of stored node. 0 - directory, 1 - file.</td></tr>
 * <tr><td>[1, 9)</td><td>Offset to content of stored node.</td></tr>
 * <tr><td>[9, 10)</td><td>Length of node's name</td></tr>
 * <tr><td>[10, 10 + name length)</td><td>Node's name</td></tr>
 * </table>
 */
@NotThreadSafe
@ParametersAreNonnullByDefault
class DirectoryNode extends Node {
    private static final int OFFSET_TO_NODE_NAME_LENGTH = 9;
    private static final int METADATA_SIZE = 10;

    DirectoryNode(FileChannelAdaptor fileChannel, Allocator allocator, long startPosition) {
        super(fileChannel, allocator, startPosition);
    }

    boolean isEmpty() {
        return makeChunkList().flipToChunkStartAndCheckIsListEnd();
    }

    /**
     * Finds node by name in current directory.
     *
     * @param name Name of the node.
     * @return Node info for the node or null if no node with specified name found.
     */
    @Nullable
    DirectoryChildInfo findNode(String name) {
        ChunkList chunkList = makeChunkList();
        while (!chunkList.flipToChunkStartAndCheckIsListEnd()) {
            ChunkList.Mark nodeMark = chunkList.mark();
            Object[] metadata = chunkList.readObjects(Byte.class, Long.class, Byte.class);
            NodeType nodeType = NodeType.values()[UnsignedBytes.toInt((byte) metadata[0])];
            long contentsOffset = (long) metadata[1];
            int nameSize = UnsignedBytes.toInt((byte) metadata[2]);
            byte[] nameRaw = chunkList.readBytesStrict(nameSize);
            String nodeName = new String(nameRaw, Charsets.UTF_8);
            if (nodeName.equals(name)) {
                switch (nodeType) {
                    case FILE:
                        return new DirectoryChildInfo(name, false, contentsOffset, nodeMark);
                    case DIRECTORY:
                        return new DirectoryChildInfo(name, true, contentsOffset, nodeMark);
                    default:
                        throw new IllegalStateException(nodeType.toString());
                }
            }
        }
        return null;
    }

    /**
     * @return List of all nodes in current directory.
     */
    List<DirectoryChildInfo> list() {
        List<DirectoryChildInfo> result = new ArrayList<>();
        ChunkList chunkList = makeChunkList();
        while (!chunkList.flipToChunkStartAndCheckIsListEnd()) {
            ChunkList.Mark nodeMark = chunkList.mark();
            NodeType nodeType = NodeType.values()[chunkList.readByte()];
            long contentsOffset = chunkList.readLong();
            int nameSize = UnsignedBytes.toInt(chunkList.readByte());
            byte[] nameRaw = chunkList.readBytesStrict(nameSize);
            String name = new String(nameRaw, Charsets.UTF_8);
            switch (nodeType) {
                case FILE:
                    result.add(new DirectoryChildInfo(name, false, contentsOffset, nodeMark));
                    break;
                case DIRECTORY:
                    result.add(new DirectoryChildInfo(name, true, contentsOffset, nodeMark));
                    break;
                default:
                    throw new IllegalStateException(nodeType.toString());
            }
        }
        return result;
    }

    /**
     * Deletes content of node, returning all used blocks to allocator.
     */
    void deallocateNode(DirectoryChildInfo node) {
        ChunkList chunkList = makeChunkList();
        node.getNodeMark().restore(chunkList);
        int shrinkSize = METADATA_SIZE;
        chunkList.forwardStrict(OFFSET_TO_NODE_NAME_LENGTH);
        shrinkSize += chunkList.readByte();
        node.getNodeMark().restore(chunkList);
        chunkList.shrink(shrinkSize);
    }

    /**
     * Adds info about new file in directory.
     *
     * @param name           Name of file.
     * @param contentsOffset Offset to file's content.
     * @return true if node was successfully added, false if node with such name already exists.
     */
    boolean addFile(String name, long contentsOffset) {
        return addNode(name, NodeType.FILE, contentsOffset);
    }

    /**
     * Adds info about new directory in directory.
     *
     * @param name           Name of new directory.
     * @param contentsOffset Offset to new directory's content.
     * @return true if node was successfully added, false if node with such name already exists.
     */
    boolean addDirectory(String name, long contentsOffset) {
        return addNode(name, NodeType.DIRECTORY, contentsOffset);
    }

    private boolean addNode(String name, NodeType nodeType, long contentsOffset) {
        byte[] nameAsBytes = name.getBytes(Charsets.UTF_8);
        if (nameAsBytes.length > 255) {
            throw new NameTooLong(nameAsBytes.length);
        }
        ChunkList chunkList = makeChunkList();
        while (!chunkList.flipToChunkStartAndCheckIsListEnd()) {
            chunkList.forwardStrict(OFFSET_TO_NODE_NAME_LENGTH);
            int cursorNameSize = UnsignedBytes.toInt(chunkList.readByte());
            if (nameAsBytes.length == cursorNameSize) {
                byte[] cursorName = chunkList.readBytesStrict(cursorNameSize);
                int i = 0;
                for (; i < cursorNameSize; ++i) {
                    if (nameAsBytes[i] != cursorName[i]) {
                        break;
                    }
                }
                if (i == cursorNameSize) {
                    return false;
                }
            } else {
                chunkList.forwardStrict(cursorNameSize);
            }
        }
        chunkList.expand(METADATA_SIZE + nameAsBytes.length);
        chunkList.replaceByte((byte) nodeType.ordinal());
        chunkList.replaceLong(contentsOffset);
        chunkList.replaceByte((byte) nameAsBytes.length);
        chunkList.replaceBytes(nameAsBytes);
        return true;
    }

    private enum NodeType {
        DIRECTORY,
        FILE,
    }
}
