package com.example.jbfs;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.ThreadSafe;

import com.google.common.base.Preconditions;

/**
 * Implementation of allocator that stores information about used and free blocks in a bit set.
 * <p>
 * This class internally uses {@link ChunkListBitSet} and highly relies on its implementation.
 */
@ThreadSafe
@ParametersAreNonnullByDefault
public class ChunkListBitSetAllocator implements Allocator {
    private final FileChannelAdaptor fileChannel;
    private final ChunkListBitSet bitSet;
    private final long headerPosition;
    private final ByteBuffer metadataBuffer = ByteBuffer.allocate(16);
    private final int blockSize;

    /**
     * Underlying {@link #bitSet} uses these allocator. This field is used to indicate special mode for allocating
     * blocks for bitset. When {@link #allocate()} tries to extend bitset then bitset internally calls {@link
     * #allocate()} recursively.
     */
    private boolean recursiveMode = false;
    private BitSetAllocator forwardAllocator;

    ChunkListBitSetAllocator(FileChannelAdaptor fileChannel, long headerPosition, long rootChunkStart,
            int blockSize, long positionLimit)
    {
        this.fileChannel = fileChannel;
        this.headerPosition = headerPosition;
        this.blockSize = blockSize;
        this.bitSet = new ChunkListBitSet(new ChunkList(fileChannel, this, rootChunkStart));
        // + 2 blockSizes: one for first chunk of bitset, one for first chunk of root directory.
        // Both two chunks can never be deallocated.
        this.forwardAllocator = new BitSetAllocator(bitSet, rootChunkStart + 2 * blockSize, blockSize, positionLimit);
    }

    public static void format(FileChannelAdaptor fileChannel, long headerPosition, long rootChunkStart,
            int blockSize)
    {
        try {
            long counter = 0;
            long capacity = ((long) blockSize - ChunkListStorage.METADATA_SIZE) * 8;
            ByteBuffer metadataBuffer = ByteBuffer.allocate(24);
            metadataBuffer.position(0);
            metadataBuffer.putLong(capacity);
            metadataBuffer.putLong(counter);
            metadataBuffer.limit(metadataBuffer.position());
            metadataBuffer.flip();
            fileChannel.write(metadataBuffer, headerPosition);
            Allocator fakeAllocator = new Allocator() {
                @Override
                public long allocate() {
                    throw new IllegalStateException();
                }

                @Override
                public void deallocate(long offset) {
                    throw new IllegalStateException();
                }

                @Override
                public int getBlockSize() {
                    return blockSize;
                }
            };
            ChunkList root = new ChunkList(fileChannel, fakeAllocator, rootChunkStart);
            root.expand((int) capacity / 8);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public synchronized long allocate() {
        try {
            metadataBuffer.position(0);
            fileChannel.read(metadataBuffer, headerPosition);
            metadataBuffer.position(0);
            long capacity = metadataBuffer.getLong();
            long counter = metadataBuffer.getLong();
            Preconditions.checkState(counter < capacity);
            if (!recursiveMode && counter + 1 == capacity) {
                recursiveMode = true;
                try {
                    int expandBy = blockSize - ChunkListStorage.METADATA_SIZE;
                    bitSet.expand(expandBy);
                    metadataBuffer.position(0);
                    fileChannel.read(metadataBuffer, headerPosition);
                    metadataBuffer.position(0);
                    capacity = metadataBuffer.getLong();
                    counter = metadataBuffer.getLong();
                    // * 8 - expandBy contains bytes, capacity contains bits
                    capacity += expandBy * 8;
                } finally {
                    recursiveMode = false;
                }
            }
            long blockPosition = forwardAllocator.allocate();
            ++counter;
            metadataBuffer.position(0);
            metadataBuffer.putLong(capacity);
            metadataBuffer.putLong(counter);
            metadataBuffer.position(0);
            fileChannel.write(metadataBuffer, headerPosition);
            return blockPosition;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public synchronized void deallocate(long value) {
        try {
            metadataBuffer.position(0);
            fileChannel.read(metadataBuffer, headerPosition);
            long counter = metadataBuffer.getLong(8);
            forwardAllocator.deallocate(value);
            --counter;
            metadataBuffer.putLong(8, counter);
            metadataBuffer.position(0);
            fileChannel.write(metadataBuffer, headerPosition);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public int getBlockSize() {
        return blockSize;
    }

    @Override
    public void setPositionLimit(long positionLimit) {
        this.forwardAllocator.setPositionLimit(positionLimit);
    }
}
