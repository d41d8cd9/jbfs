package com.example.jbfs;

import java.util.ArrayDeque;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Safely releases list of resources despite of thrown errors.
 */
@ParametersAreNonnullByDefault
public class CloseableList<T extends AutoCloseable>
        extends ArrayDeque<T>
        implements AutoCloseable
{
    @Override
    public void close() {
        IllegalStateException rootExc = new IllegalStateException("Some items was not closed correctly");
        for (T object : this) {
            if (object != null) {
                try {
                    object.close();
                } catch (Exception objExc) {
                    rootExc.addSuppressed(objExc);
                }
            }
        }
        if (rootExc.getSuppressed().length > 0) {
            throw rootExc;
        }
    }
}
