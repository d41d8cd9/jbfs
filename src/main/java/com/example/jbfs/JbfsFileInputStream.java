package com.example.jbfs;

import java.io.IOException;
import java.io.InputStream;

import com.google.common.primitives.UnsignedBytes;

public class JbfsFileInputStream extends InputStream {
    private final ChunkList chunkList;
    private final BlockGuard blockGuard;
    private ChunkList.Mark markOfChunkStart;
    private ChunkList.Mark userRequestedMark;
    private int userRequestedMarkOffset;
    private byte[] readAheadBuffer = new byte[0];
    private int readAheadBufferPosition;

    JbfsFileInputStream(ChunkList chunkList, BlockGuard blockGuard) {
        this.chunkList = chunkList;
        this.blockGuard = blockGuard;
    }

    @Override
    public void close() {
        blockGuard.close();
    }

    @Override
    public int read() {
        if (readAheadBufferPosition == readAheadBuffer.length) {
            if (chunkList.flipToChunkStartAndCheckIsListEnd()) {
                return -1;
            }
            markOfChunkStart = chunkList.mark();
            readAheadBuffer = chunkList.readBytesUntilChunkEnd();
            assert readAheadBuffer.length > 0;
            readAheadBufferPosition = 0;
        }
        int result = UnsignedBytes.toInt(readAheadBuffer[readAheadBufferPosition]);
        ++readAheadBufferPosition;
        return result;
    }

    @Override
    public long skip(long n) {
        if (readAheadBufferPosition + n < readAheadBuffer.length) {
            readAheadBufferPosition += n;
            return n;
        } else {
            int alreadyReadAhead = readAheadBuffer.length - readAheadBufferPosition;
            readAheadBuffer = new byte[0];
            readAheadBufferPosition = 0;
            return chunkList.forward(n - alreadyReadAhead) + alreadyReadAhead;
        }
    }

    @Override
    public synchronized void mark(int readlimit) {
        userRequestedMark = markOfChunkStart;
        userRequestedMarkOffset = readAheadBufferPosition;
    }

    @Override
    public synchronized void reset() throws IOException {
        if (userRequestedMark == null) {
            throw new IOException("Mark was not set");
        }
        userRequestedMark.close();
        chunkList.forward(userRequestedMarkOffset);
        readAheadBuffer = new byte[0];
        readAheadBufferPosition = 0;
    }

    @Override
    public boolean markSupported() {
        return true;
    }
}
