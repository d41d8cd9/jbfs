package com.example.jbfs;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.ThreadSafe;

/**
 * Adaptor for {@link FileChannel} that forwards subset of FileChannel's methods
 * and guarantees thread safety of each forwarded operations.
 */
@ParametersAreNonnullByDefault
@ThreadSafe
class FileChannelAdaptor {
    private final FileChannel fileChannel;

    FileChannelAdaptor(FileChannel fileChannel) {
        this.fileChannel = fileChannel;
    }

    /**
     * Writes byte buffer at specified position.
     *
     * @param src      Data source.
     * @param position Offset in backend file.
     * @return Count of written bytes.
     */
    synchronized int write(ByteBuffer src, long position) throws IOException {
        return fileChannel.write(src, position);
    }

    /**
     * Reads byte buffer at specified position.
     *
     * @param dst      Data sink.
     * @param position Offset in backend file.
     * @return Count of read bytes.
     */
    synchronized int read(ByteBuffer dst, long position) throws IOException {
        return fileChannel.read(dst, position);
    }

    /**
     * @return Size in bytes of backend file.
     */
    long getSize() {
        try {
            return fileChannel.size();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * Truncates backend file to the new size.
     *
     * @param size Size in bytes.
     */
    synchronized void resize(long size) {
        try {
            fileChannel.truncate(size);
            fileChannel.position(size - 1);
            fileChannel.write(ByteBuffer.wrap(new byte[]{0}));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * Closes underlying FileChannel.
     */
    public synchronized void close() throws IOException {
        fileChannel.close();
    }
}
