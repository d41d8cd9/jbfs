package com.example.jbfs;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Creates instances of {@link BlockGuard} for blocks.
 */
@ParametersAreNonnullByDefault
public interface BlockGuardFactory {
    /**
     * Locks specified block. If the block is already locked by another thread, then waits until the block will be
     * unlocked.
     *
     * @param offset Offset to start of desired block.
     * @param kind   Exclusive or shared lock.
     * @return {@link BlockGuard} that used to unlock the block back.
     * @throws InterruptedException When awaiting for the lock is interrupted.
     */
    BlockGuard acquire(long offset, Kind kind) throws InterruptedException;

    enum Kind {
        EXCLUSIVE,
        SHARED
    }
}
