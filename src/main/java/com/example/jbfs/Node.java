package com.example.jbfs;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Node that may be stored in a directory.
 */
@ParametersAreNonnullByDefault
abstract class Node {
    private final FileChannelAdaptor fileChannel;
    private final Allocator allocator;
    private final long startPosition;

    Node(FileChannelAdaptor fileChannel, Allocator allocator, long startPosition) {
        this.fileChannel = fileChannel;
        this.allocator = allocator;
        this.startPosition = startPosition;
    }

    /** Returns chunk list for node's content. */
    ChunkList makeChunkList() {
        return new ChunkList(fileChannel, allocator, startPosition);
    }
}
