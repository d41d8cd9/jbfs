package com.example.jbfs;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;

import javax.annotation.ParametersAreNonnullByDefault;

import com.google.common.base.Preconditions;

@ParametersAreNonnullByDefault
public interface ChunkListStorage {
    int METADATA_SIZE = 20;

    static void writeEmptyChunkHeader(FileChannelAdaptor fileChannel, long startPosition) {
        try {
            ByteBuffer byteBuffer = ByteBuffer.allocate(METADATA_SIZE);
            fileChannel.write(byteBuffer, startPosition);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * @return Offset from start of file where current position is.
     */
    long getPosition();

    /**
     * Sets current position to specified.
     */
    void jumpToPosition(long newPosition);

    /**
     * Reads byte from current position and increments current position.
     */
    byte readByte();

    /**
     * Reads byte at specified position. Does not alter current position.
     */
    byte readByteAt(long position);

    /**
     * Writes byte at current position and increments current position.
     */
    void writeByte(byte data);

    /**
     * Reads amount of bytes from current position and increment current position.
     *
     * @param data Where read data should be placed.
     * @throws IllegalStateException If reached end of chunk list before all requested bytes was read.
     */
    default void readBytesStrict(byte[] data, int offset, int length) {
        int readLength = readBytes(data, offset, length);
        Preconditions.checkState(readLength == length,
                "Expected to read %s bytes but read %s bytes", length, readLength);
    }

    /**
     * Reads amount of bytes from current position and increment current position.
     *
     * @param data   Where read data should be placed.
     * @param offset Offset from start of array.
     * @param length Count of bytes from offset.
     * @return Count of really read bytes.
     */
    int readBytes(byte[] data, int offset, int length);

    /**
     * Reads amount of bytes from current position and increment current position.
     *
     * @param data Where read data should be placed.
     * @throws IllegalStateException If reached end of chunk list before all requested bytes was read.
     */
    default void readBytesStrict(byte[] data) {
        readBytesStrict(data, 0, data.length);
    }

    /**
     * Writes all data at current position and increment current position.
     *
     * @param data   New bytes.
     * @param offset Offset from start of data.
     * @param length Count of bytes from offset.
     */
    void writeBytes(byte[] data, int offset, int length);

    /**
     * Writes all data at current position and increment current position.
     *
     * @param data New bytes.
     */
    default void writeBytes(byte[] data) {
        writeBytes(data, 0, data.length);
    }

    /**
     * Writes zero bytes at current position.
     *
     * @param count How many zero bytes to write.
     */
    void clearBytes(int count);

    /**
     * @return Offset in backend storage to the start of current chunk payload.
     */
    long getCurrentChunkDataStart();

    /**
     * @return Offset in backend storage to the start of the next chunk or zero if this is the last chunk in list.
     */
    long getNextChunkOffset();

    /**
     * Writes offset in backend storage of next chunk for some another chunk.
     *
     * @param forChunk        Offset in backend storage where some chunk starts.
     * @param nextChunkOffset Offset in backend storage for next chunk of some chunk.
     */
    void writeNextChunkOffsetFor(long forChunk, long nextChunkOffset);

    /**
     * @return Offset in backend storage to the start of the previous chunk or zero if this is the first chunk in list.
     */
    long getPreviousChunkOffset();

    /**
     * @return Size of payload of current chunk.
     */
    int getCurrentChunkDataSize();

    /**
     * Stores size of payload of current chunk.
     */
    void writeCurrentChunkDataSize(int currentChunkDataSize);

    /**
     * Reduces size of payload of current chunk.
     *
     * @param diff How many bytes to cut off from the end of chunk.
     */
    void decreaseCurrentChunkDataSize(int diff);

    /**
     * @return Offset in backend storage to the end of payload of current chunk.
     */
    long getCurrentChunkDataEnd();

    /**
     * Writes offset in backend storage of previous chunk for some another chunk.
     *
     * @param forChunk            Offset in backend storage where some chunk starts.
     * @param previousChunkOffset Offset in backend storage for previous chunk of some chunk.
     */
    void writePreviousChunkPositionFor(long forChunk, long previousChunkOffset);

    /**
     * @return Offset in backend storage to start of current chunk.
     */
    long getChunkStartPosition();

    /**
     * Sets current position to start of specified chunk and reads chunk header.
     *
     * @param chunkStartPosition Start of desired chunk.
     */
    void jumpToChunk(long chunkStartPosition);

    /**
     * Writes in backend storage size of payload of some chunk. of previous chunk for some another chunk.
     *
     * @param forChunk Offset in backend storage where some chunk starts.
     * @param dataSize Payload size for those chunk.
     */
    void writeCurrentChunkDataSizeFor(long forChunk, int dataSize);
}
