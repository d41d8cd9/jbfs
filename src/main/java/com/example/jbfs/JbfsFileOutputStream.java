package com.example.jbfs;

import java.io.OutputStream;

import javax.annotation.ParametersAreNonnullByDefault;

import com.google.common.base.Preconditions;

@ParametersAreNonnullByDefault
public class JbfsFileOutputStream extends OutputStream {
    private final ChunkList chunkList;
    private final BlockGuard blockGuard;

    JbfsFileOutputStream(ChunkList chunkList, BlockGuard blockGuard) {
        this.chunkList = chunkList;
        this.blockGuard = blockGuard;
    }

    ChunkList getChunkList() {
        return chunkList;
    }

    @Override
    public void close() {
        blockGuard.close();
    }

    @Override
    public void write(int b) {
        if (chunkList.flipToChunkStartAndCheckIsListEnd()) {
            chunkList.expand(1);
        }
        chunkList.replaceByte((byte) b);
    }

    @Override
    public void write(byte[] data, int off, int len) {
        int writtenBeforeChunkListEnd = chunkList.replaceBytes(data, off, len);
        off += writtenBeforeChunkListEnd;
        len -= writtenBeforeChunkListEnd;
        if (len > 0) {
            chunkList.expand(len);
            writtenBeforeChunkListEnd = chunkList.replaceBytes(data, off, len);
            Preconditions.checkState(writtenBeforeChunkListEnd == len, "%s != %s",
                    writtenBeforeChunkListEnd, len);
        }
    }
}
