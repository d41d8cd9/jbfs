package com.example.jbfs;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Represents a file in a directory. Unlike {@link DirectoryNode} does not have header.
 */
@ParametersAreNonnullByDefault
class FileNode extends Node {
    FileNode(FileChannelAdaptor fileChannel, Allocator allocator, long startPosition) {
        super(fileChannel, allocator, startPosition);
    }
}
