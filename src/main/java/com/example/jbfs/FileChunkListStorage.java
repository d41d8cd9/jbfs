package com.example.jbfs;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.NotThreadSafe;

import com.google.common.base.Preconditions;

/**
 * Base of {@link ChunkList} that used to encapsulate byte arithmetic.
 */
@NotThreadSafe
@ParametersAreNonnullByDefault
class FileChunkListStorage implements ChunkListStorage {
    private static final ByteBuffer ZERO_BYTES = ByteBuffer.allocate(8).asReadOnlyBuffer();
    private final FileChannelAdaptor channel;
    private final ByteBuffer temporaryStorage;
    /**
     * Can't obey position from fileChannel, it may be shared between several routines, i.e. while reading two files
     * simultaneously. Each backend need to have their own position.
     */
    private long filePosition;
    private long currentChunkDataStart;
    private long nextChunkOffset;
    private long previousChunkOffset;
    private int currentChunkDataSize;

    FileChunkListStorage(FileChannelAdaptor channel) {
        this.channel = channel;
        temporaryStorage = ByteBuffer.allocate(METADATA_SIZE);
        temporaryStorage.order(ByteOrder.BIG_ENDIAN);
    }

    @Override
    public long getPosition() {
        return filePosition;
    }

    @Override
    public void jumpToPosition(long newPosition) {
        filePosition = newPosition;
    }

    @Override
    public byte readByte() {
        byte result = readByteAt(filePosition);
        ++filePosition;
        return result;
    }

    @Override
    public byte readByteAt(long position) {
        temporaryStorage.position(0);
        temporaryStorage.limit(1);
        try {
            int count = channel.read(temporaryStorage, position);
            checkReadCount(count, 1);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return temporaryStorage.get(0);
    }

    @Override
    public void writeByte(byte data) {
        temporaryStorage.position(0);
        temporaryStorage.limit(1);
        temporaryStorage.put(0, data);
        try {
            int count = channel.write(temporaryStorage, filePosition);
            filePosition += count;
            checkWriteCount(count, 1);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private void checkReadCount(int count, int expected) {
        Preconditions.checkState(count == expected,
                "Expected to read %s bytes but read %s bytes", expected, count);
    }

    private void checkWriteCount(int count, int expected) {
        Preconditions.checkState(count == expected,
                "Expected to write %s bytes but written %s bytes", expected, count);
    }

    @Override
    public int readBytes(byte[] data, int offset, int length) {
        try {
            int count = channel.read(ByteBuffer.wrap(data, offset, length), filePosition);
            filePosition += count;
            return count;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public void writeBytes(byte[] data, int offset, int length) {
        try {
            int count = channel.write(ByteBuffer.wrap(data, offset, length), filePosition);
            filePosition += count;
            checkWriteCount(count, length);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public void clearBytes(int count) {
        try {
            while (count >= ZERO_BYTES.limit()) {
                int written = channel.write(ZERO_BYTES.duplicate(), filePosition);
                filePosition += written;
                checkWriteCount(written, ZERO_BYTES.limit());
                count -= written;
            }
            if (count != 0) {
                ByteBuffer zeros = ZERO_BYTES.duplicate();
                zeros.limit(count);
                int written = channel.write(zeros, filePosition);
                filePosition += written;
                checkWriteCount(written, count);
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public long getCurrentChunkDataStart() {
        return currentChunkDataStart;
    }

    @Override
    public long getNextChunkOffset() {
        return nextChunkOffset;
    }

    private void writeInt(long position, int data) {
        temporaryStorage.position(0);
        temporaryStorage.limit(4);
        temporaryStorage.putInt(0, data);
        try {
            int count = channel.write(temporaryStorage, position);
            checkWriteCount(count, 4);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private void writeLong(long position, long data) {
        temporaryStorage.position(0);
        temporaryStorage.limit(8);
        temporaryStorage.putLong(0, data);
        try {
            int count = channel.write(temporaryStorage, position);
            checkWriteCount(count, 8);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public void writeNextChunkOffsetFor(long forChunk, long nextChunkOffset) {
        writeLong(forChunk, nextChunkOffset);
    }

    @Override
    public long getPreviousChunkOffset() {
        return previousChunkOffset;
    }

    @Override
    public int getCurrentChunkDataSize() {
        return currentChunkDataSize;
    }

    @Override
    public void writeCurrentChunkDataSize(int currentChunkDataSize) {
        this.currentChunkDataSize = currentChunkDataSize;
        writeInt(currentChunkDataStart - 4, currentChunkDataSize);
    }

    @Override
    public void decreaseCurrentChunkDataSize(int diff) {
        writeCurrentChunkDataSize(currentChunkDataSize - diff);
    }

    @Override
    public long getCurrentChunkDataEnd() {
        return currentChunkDataStart + currentChunkDataSize;
    }

    @Override
    public void writePreviousChunkPositionFor(long forChunk, long previousChunkOffset) {
        writeLong(forChunk + 8, previousChunkOffset);
    }

    @Override
    public long getChunkStartPosition() {
        return currentChunkDataStart - METADATA_SIZE;
    }

    @Override
    public void jumpToChunk(final long chunkStartPosition) {
        Preconditions.checkState(chunkStartPosition != 0, "This implementation does not support chunks with address 0");
        jumpToPosition(chunkStartPosition);
        temporaryStorage.position(0);
        temporaryStorage.limit(METADATA_SIZE);
        try {
            int count = channel.read(temporaryStorage, filePosition);
            filePosition += count;
            checkReadCount(count, METADATA_SIZE);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        temporaryStorage.position(0);
        nextChunkOffset = temporaryStorage.getLong();
        previousChunkOffset = temporaryStorage.getLong();
        currentChunkDataSize = temporaryStorage.getInt();
        currentChunkDataStart = filePosition;
        assert currentChunkDataStart - chunkStartPosition == METADATA_SIZE;
    }

    @Override
    public void writeCurrentChunkDataSizeFor(long forChunk, int dataSize) {
        writeInt(forChunk + 16, dataSize);
    }
}
