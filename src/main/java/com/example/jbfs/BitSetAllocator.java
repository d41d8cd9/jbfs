package com.example.jbfs;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.ThreadSafe;

import com.example.jbfs.exceptions.NoSpaceLeft;
import com.google.common.base.Preconditions;

/**
 * Implementation of allocator that stores information about used and free blocks in a bit set.
 */
@ThreadSafe
@ParametersAreNonnullByDefault
public class BitSetAllocator implements Allocator {
    private final FsBitSet bitSet;
    private final long blockBufferOffset;
    private final int blockSize;
    private long positionLimit;
    private long quickPositionForNextFreeBlock;

    /**
     * @param bitSet            Underlying bitset
     * @param blockBufferOffset First possible block offset from start of backend file
     * @param blockSize         Size of block
     * @param positionLimit     Last possible offset in backend file
     */
    public BitSetAllocator(FsBitSet bitSet, long blockBufferOffset, int blockSize, long positionLimit) {
        this.bitSet = bitSet;
        this.blockBufferOffset = blockBufferOffset;
        this.blockSize = blockSize;
        this.positionLimit = positionLimit;
    }

    @Override
    public synchronized long allocate() {
        long blockId;
        long result;
        do {
            blockId = bitSet.nextClearBit(quickPositionForNextFreeBlock);
            result = blockBufferOffset + blockId * blockSize;
            if (blockId == -1 || result + blockSize >= positionLimit) {
                blockId = -1;
                if (quickPositionForNextFreeBlock == 0) {
                    throw new NoSpaceLeft();
                } else {
                    quickPositionForNextFreeBlock = 0;
                }
            }
        } while (blockId == -1);
        Preconditions.checkState(!bitSet.get(blockId), "double acquire of block %s (offset %s)", blockId, result);
        bitSet.set(blockId, true);
        quickPositionForNextFreeBlock = blockId;
        return result;
    }

    @Override
    public synchronized void deallocate(long value) {
        long blockId = (value - blockBufferOffset) / blockSize;
        Preconditions.checkState(bitSet.get(blockId), "double free");
        bitSet.set(blockId, false);
    }

    @Override
    public int getBlockSize() {
        return blockSize;
    }

    @Override
    public void setPositionLimit(long positionLimit) {
        this.positionLimit = positionLimit;
    }
}
