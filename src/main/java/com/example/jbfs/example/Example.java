package com.example.jbfs.example;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.annotation.ParametersAreNonnullByDefault;

import com.example.jbfs.DirectoryChildInfo;
import com.example.jbfs.FileBackedFileSystem;
import com.example.jbfs.exceptions.FileOrDirectoryExists;
import com.example.jbfs.exceptions.JbfsException;
import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ParametersAreNonnullByDefault
@SuppressWarnings({
        "squid:S1075",  // file paths allowed here
        "squid:S1192",  // copypasted strings
        "squid:S2629",  // logging.info is always enabled
        "squid:S3776",  // cognitive complexity
        "squid:S1141",  // nested try blocks
})
public class Example {
    private static final int BLOCK_SIZE = 256;
    private static final long BLOCK_COUNT = 512;
    private static final long NEW_FS_SIZE = 1 << 22;
    private static final Logger logger = LoggerFactory.getLogger(Example.class);

    private static void example(Path backendFile) throws IOException, InterruptedException {
        logger.info("Backend file is {}", backendFile);

        logger.info("Formatting new file system (block size is {}, block count is {}) and closing file",
                BLOCK_SIZE, BLOCK_COUNT);
        FileBackedFileSystem.format(backendFile, NEW_FS_SIZE, BLOCK_SIZE);

        singleThreadExample(backendFile);

        multiThreadExample(backendFile);
    }

    private static void singleThreadExample(Path backendFile) throws IOException, InterruptedException {
        final String path1 = "/example.txt";
        final String path2 = "/subdir";
        final String path3 = "/subdir/new_example.txt";
        final String path4 = "/sgdhdgdsgfds";
        try (FileBackedFileSystem fileSystem = FileBackedFileSystem.mount(backendFile)) {
            listDirectory(fileSystem, "/");

            logger.info("Creating file {}", path1);
            fileSystem.makeFile(path1);
            listDirectory(fileSystem, "/");

            logger.info("Writing some data to file {}", path1);
            try (Writer writer = new OutputStreamWriter(fileSystem.openForWriting(path1))) {
                writer.write("Hello");
                writer.write(" ");
                writer.flush();
                writer.write("world");
                writer.write("!");
            }
        }

        // filesystem may be unmounted and mounted at any time
        try (FileBackedFileSystem fileSystem = FileBackedFileSystem.mount(backendFile)) {
            readFile(fileSystem, path1);

            logger.info("Creating directory {}", path2);
            fileSystem.makeDirectory(path2);
            listDirectory(fileSystem, "/");
            listDirectory(fileSystem, path2);

            logger.info("Creating file {}", path3);
            fileSystem.makeFile(path3);
            listDirectory(fileSystem, "/subdir");

            logger.info("Appending some data to file {}", path1);
            try (Writer writer = new OutputStreamWriter(fileSystem.openForAppending(path1))) {
                writer.write(" ");
                writer.write("Hello again!");
            }
        }

        // filesystem may be unmounted and mounted at any time
        try (FileBackedFileSystem fileSystem = FileBackedFileSystem.mount(backendFile)) {
            logger.info("Simultaneously opening {} for reading, {} for writing and copying streams",
                    path1, path3);
            try (InputStream readStream = fileSystem.openForReading(path1);
                 OutputStream writeStream = fileSystem.openForWriting(path3))
            {
                IOUtils.copy(readStream, writeStream);
            }

            readFile(fileSystem, path3);

            logger.info("Getting contents of directory {}", path4);
            expectError(() -> listDirectory(fileSystem, path4));

            logger.info("Reading file {}", path4);
            expectError(() -> fileSystem.openForReading(path4));

            logger.info("Deleting file {}", path1);
            fileSystem.deleteFile(path1);
            listDirectory(fileSystem, "/");

            logger.info("Deleting directory {}", path2);
            expectError(() -> fileSystem.deleteDirectory(path2));

            logger.info("Deleting file {}", path3);
            fileSystem.deleteFile(path3);
            listDirectory(fileSystem, path2);

            logger.info("Deleting directory {}", path2);
            fileSystem.deleteDirectory(path2);
            listDirectory(fileSystem, "/");
        }
    }

    private static void multiThreadExample(Path backendFile) throws IOException, InterruptedException {
        try (FileBackedFileSystem fileSystem = FileBackedFileSystem.mount(backendFile)) {
            ExecutorService executorService = Executors.newFixedThreadPool(4);
            try {
                int count = 10;
                logger.info("Creating {} files like /subdir/file<n>", count);
                CompletableFuture.allOf(IntStream.range(1, count + 1)
                        .mapToObj(i -> CompletableFuture.runAsync(
                                () -> {
                                    try {
                                        logger.info("{}: mkdir /subdir", i);
                                        try {
                                            fileSystem.makeDirectory("/subdir");
                                            logger.info("{}: Success mkdir /subdir", i);
                                        } catch (FileOrDirectoryExists exc) {
                                            logger.info("{}: Error mkdir /subdir: {}", i, exc.getMessage());
                                        }
                                        logger.info("{}: touch /subdir/file{}", i, i);
                                        String filePath = "/subdir/file" + i;
                                        fileSystem.makeFile(filePath);
                                        logger.info("{}: echo {} > /subdir/file{}", i, i, i);
                                        try (OutputStream stream = fileSystem.openForWriting(filePath)) {
                                            IOUtils.write(
                                                    String.format(">>> %d <<< ", i), stream, StandardCharsets.UTF_8);
                                        } catch (IOException e) {
                                            throw new UncheckedIOException(e);
                                        }
                                    } catch (InterruptedException exc) {
                                        Thread.currentThread().interrupt();
                                        throw new IllegalStateException(exc);
                                    } catch (RuntimeException exc) {
                                        throw new IllegalStateException("Bug for " + i, exc);
                                    }
                                },
                                executorService))
                        .toArray(CompletableFuture[]::new))
                        .join();

                listDirectory(fileSystem, "/");
                listDirectory(fileSystem, "/subdir");

                logger.info("Concurrently writing and reading files in /subdir");
                List<String> subdirFiles = fileSystem.listDirectory("/subdir").stream()
                        .filter(DirectoryChildInfo::isFile)
                        .map(DirectoryChildInfo::getName)
                        .map(n -> "/subdir/" + n)
                        .collect(ImmutableList.toImmutableList());
                CompletableFuture.allOf(IntStream.range(1, count + 1)
                        .mapToObj(id -> CompletableFuture.runAsync(
                                () -> {
                                    List<String> checkingSubdirFiles = new ArrayList<>(subdirFiles);
                                    Collections.shuffle(checkingSubdirFiles);
                                    checkingSubdirFiles.forEach(subdirFile -> {
                                        try {
                                            String content;
                                            logger.info("{}: Reading {}", id, subdirFile);
                                            try (InputStream stream = fileSystem.openForReading(subdirFile)) {
                                                content = IOUtils.toString(stream, StandardCharsets.UTF_8);
                                            }
                                            logger.info("{}: Read from {}: {}", id, subdirFile, content);

                                            logger.info("{}: Appending self-description to {}", id, subdirFile);
                                            try (OutputStream stream = fileSystem.openForAppending(subdirFile)) {
                                                IOUtils.write(id + " was here. ", stream, StandardCharsets.UTF_8);
                                            }

                                            logger.info("{}: Reading after first append {}", id, subdirFile);
                                            try (InputStream stream = fileSystem.openForReading(subdirFile)) {
                                                content = IOUtils.toString(stream, StandardCharsets.UTF_8);
                                            }
                                            logger.info("{}: Read from {} after first append : {}",
                                                    id, subdirFile, content);

                                            Thread.sleep(ThreadLocalRandom.current().nextInt(1000));

                                            logger.info("{}: Appending again self-description to {}", id, subdirFile);
                                            try (OutputStream stream = fileSystem.openForAppending(subdirFile)) {
                                                IOUtils.write(id + " was here again. ", stream, StandardCharsets.UTF_8);
                                            }

                                            logger.info("{}: Reading after second append {}", id, subdirFile);
                                            try (InputStream stream = fileSystem.openForReading(subdirFile)) {
                                                content = IOUtils.toString(stream, StandardCharsets.UTF_8);
                                            }
                                            logger.info("{}: Read from {} after second append: {}",
                                                    id, subdirFile, content);
                                        } catch (InterruptedException exc) {
                                            Thread.currentThread().interrupt();
                                            throw new IllegalStateException(exc);
                                        } catch (IOException exc) {
                                            throw new UncheckedIOException(exc);
                                        }
                                    });
                                },
                                executorService))
                        .toArray(CompletableFuture[]::new))
                        .join();
                subdirFiles.forEach(fileName -> {
                    try (InputStream stream = fileSystem.openForReading(fileName)) {
                        String content = IOUtils.toString(stream, StandardCharsets.UTF_8);
                        logger.info("cat {}: {}", fileName, content);
                    } catch (InterruptedException exc) {
                        Thread.currentThread().interrupt();
                        throw new IllegalStateException(exc);
                    } catch (IOException exc) {
                        throw new UncheckedIOException(exc);
                    }
                });

                logger.info("Deleting everything");
                CompletableFuture.allOf(subdirFiles.stream()
                        .map(fileName -> CompletableFuture.runAsync(
                                () -> {
                                    try {
                                        logger.info("Removing {}", fileName);
                                        fileSystem.deleteFile(fileName);
                                        listDirectory(fileSystem, "/");

                                        logger.info("Deleting /subdir");
                                        try {
                                            fileSystem.deleteDirectory("/subdir");
                                            logger.info("Deleted /subdir");
                                        } catch (JbfsException exc) {
                                            logger.info("Ooops, {}", exc.getMessage());
                                        }
                                    } catch (InterruptedException exc) {
                                        Thread.currentThread().interrupt();
                                        throw new IllegalStateException(exc);
                                    }
                                },
                                executorService))
                        .toArray(CompletableFuture[]::new))
                        .join();
            } finally {
                executorService.shutdownNow();
            }
            try {
                executorService.awaitTermination(10, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new IllegalStateException(e);
            }
        }
    }

    private static void expectError(InterruptibleRunnable runnable) throws InterruptedException {
        try {
            runnable.run();
            logger.error("Sadly, previous action did not throw expected error.");
        } catch (JbfsException err) {
            logger.info("Oops, {}", err.getMessage());
        }
    }

    private static void readFile(FileBackedFileSystem fileSystem, String path)
            throws IOException, InterruptedException
    {
        logger.info("Reading file {}", path);
        try (InputStream stream = fileSystem.openForReading(path)) {
            String content = IOUtils.toString(stream, Charsets.US_ASCII);
            logger.info("Content: {}", content);
        }
    }

    private static void listDirectory(FileBackedFileSystem fileSystem, String path) throws InterruptedException {
        List<DirectoryChildInfo> listDir = fileSystem.listDirectory(path);
        logger.info("ls {}: [{}]", path, listDir.stream()
                .map(n -> n.getName() + (n.isDirectory() ? "/" : ""))
                .collect(Collectors.joining(", ")));
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length > 0) {
            example(Paths.get(args[0]));
        } else {
            Path backendFile = Files.createTempFile("jbfs-example-", "").toAbsolutePath();
            try {
                example(backendFile);
            } finally {
                Files.delete(backendFile);
            }
        }
    }

    @FunctionalInterface
    interface InterruptibleRunnable {
        void run() throws InterruptedException;
    }
}
