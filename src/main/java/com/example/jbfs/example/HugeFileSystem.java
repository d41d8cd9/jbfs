package com.example.jbfs.example;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.annotation.ParametersAreNonnullByDefault;

import com.example.jbfs.FileBackedFileSystem;
import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ParametersAreNonnullByDefault
@SuppressWarnings("squid:S106")  // System.err
public class HugeFileSystem {
    private static final Logger logger = LoggerFactory.getLogger(HugeFileSystem.class);
    private static final int BLOCK_SIZE = 1 << 16;
    private static final long NEW_FS_SIZE = 1L << 35;  // 32 Gb

    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length > 0) {
            runTest(Paths.get(args[0]));
        } else {
            Path backendFile = Files.createTempFile("jbfs-huge-", "").toAbsolutePath();
            try {
                runTest(backendFile);
            } finally {
                Files.delete(backendFile);
            }
        }
    }

    private static void runTest(Path backendFile) throws IOException, InterruptedException {
        logger.info("Backend file is {}", backendFile);

        logger.info("Formatting new file system (block size is {}, block count is {}) and closing file", BLOCK_SIZE);
        FileBackedFileSystem.format(backendFile, NEW_FS_SIZE, BLOCK_SIZE);

        try (FileBackedFileSystem fileSystem = FileBackedFileSystem.mount(backendFile)) {
            byte[] contents = "This is content of a small file".getBytes();
            filesInOneDirectory(fileSystem, 500, contents, true);
            filesInOneDirectory(fileSystem, 2_000, contents, true);

            int contentSize = 1 << 24;
            System.err.println(String.format("Generating huge file contents with size %d bytes", contentSize));
            byte[] data = new byte[contentSize];
            for (int i = 1; i <= contentSize; ++i) {
                data[i - 1] = (byte) (i & 0xff);
            }
            filesInOneDirectory(fileSystem, 200, data, false);

            contentSize = 1 << 27;
            System.err.println(String.format("Generating huge file contents with size %d bytes", contentSize));
            data = new byte[contentSize];
            for (int i = 1; i <= contentSize; ++i) {
                data[i - 1] = (byte) (i & 0xff);
            }
            filesInOneDirectory(fileSystem, 200, data, false);
        }
    }

    private static void filesInOneDirectory(FileBackedFileSystem fileSystem, int count, byte[] content,
            boolean byteByByteBenchmark)
            throws InterruptedException, IOException
    {
        System.err.printf("Start benchmark: create %d files%n", count);
        try (Benchmark benchmark = new Benchmark(
                String.format("Create %d files with average size %d", count, content.length), Duration.ofMinutes(2)))
        {
            final List<String> files = IntStream.range(0, count)
                    .mapToObj(i -> "/file" + i)
                    .collect(Collectors.toList());
            List<String> tmp;

            List<String> createdFiles = benchmark.run("Create file", files, fileSystem::makeFile);

            List<String> writtenFiles = benchmark.run("Sequential one-shot file write", createdFiles, name -> {
                try (OutputStream stream = fileSystem.openForWriting(name)) {
                    stream.write(content);
                }
            });

            tmp = benchmark.run("Sequential buffered file write", createdFiles, name -> {
                try (OutputStream stream = new BufferedOutputStream(fileSystem.openForWriting(name))) {
                    stream.write(content);
                }
            });
            writtenFiles = Stream.concat(writtenFiles.stream(), tmp.stream())
                    .sorted()
                    .distinct()
                    .collect(Collectors.toList());

            if (byteByByteBenchmark) {
                tmp = benchmark.run("Sequential byte by byte write", createdFiles, name -> {
                    try (OutputStream stream = fileSystem.openForWriting(name)) {
                        for (byte b : content) {
                            stream.write(b);
                        }
                    }
                });
                writtenFiles = Stream.concat(writtenFiles.stream(), tmp.stream())
                        .sorted()
                        .distinct()
                        .collect(Collectors.toList());
            }

            benchmark.run("Sequential buffered file read", writtenFiles, name -> {
                try (InputStream stream = new BufferedInputStream(fileSystem.openForReading(name))) {
                    checkContentEquals(stream, new ByteArrayInputStream(content));
                }
            });

            if (byteByByteBenchmark) {
                benchmark.run("Sequential byte by byte file read", writtenFiles, name -> {
                    try (InputStream stream = fileSystem.openForReading(name)) {
                        checkContentEquals(stream, new ByteArrayInputStream(content));
                    }
                });
            }

            tmp = new ArrayList<>(createdFiles);
            Collections.shuffle(tmp);
            writtenFiles = benchmark.run("Random write", tmp, name -> {
                try (OutputStream stream = fileSystem.openForWriting(name)) {
                    stream.write(content);
                }
            });

            Collections.shuffle(writtenFiles);
            benchmark.run("Random read", writtenFiles, name -> {
                try (InputStream stream = new BufferedInputStream(fileSystem.openForReading(name))) {
                    checkContentEquals(stream, new ByteArrayInputStream(content));
                }
            });

            createdFiles.sort(Comparator.naturalOrder());
            benchmark.run("Sequential delete", createdFiles.subList(0, files.size() / 2), fileSystem::deleteFile);

            tmp = createdFiles.subList(files.size() / 2, files.size());
            Collections.shuffle(tmp);
            benchmark.run("Random delete", tmp, fileSystem::deleteFile);
        }
    }

    private static void checkContentEquals(InputStream input1, InputStream input2) throws IOException {
        int position = 0;
        int b1;
        int b2;
        while ((b1 = input1.read()) == (b2 = input2.read()) && b1 >= 0) {
            ++position;
        }
        Preconditions.checkState(b1 < 0 && b2 < 0, "Mismatch at position %s: %s != %s", position, b1, b2);
    }

    @FunctionalInterface
    interface IOConsumer<T> {
        void accept(T value) throws InterruptedException, IOException;
    }

    private static class Benchmark implements AutoCloseable {
        private final Map<String, List<Duration>> durationsByTag = new LinkedHashMap<>();
        private final String title;
        private final Duration maxTime;

        Benchmark(String title, Duration maxTime) {
            this.title = title;
            this.maxTime = maxTime;
        }

        <T> List<T> run(String tag, Collection<T> items, IOConsumer<T> consumer) throws InterruptedException, IOException {
            System.err.printf("Benchmark: %s...%n", tag);
            Duration globalStart = Duration.ofNanos(System.nanoTime());
            List<T> handledItems = new ArrayList<>();
            for (T item : items) {
                long start = System.nanoTime();
                consumer.accept(item);
                long end = System.nanoTime();
                handledItems.add(item);
                durationsByTag.computeIfAbsent(tag, k -> new ArrayList<>()).add(Duration.ofNanos(end - start));
                if (maxTime.minus(Duration.ofNanos(System.nanoTime()).minus(globalStart)).isNegative()) {
                    break;
                }
            }
            return handledItems;
        }

        @Override
        public void close() {
            System.err.printf("====== %s ======%n", title);
            durationsByTag.forEach((tag, durations) -> {
                durations.sort(Comparator.naturalOrder());
                int size = durations.size();
                System.err.printf(
                        "%s:%n    count=%s%n    min=%s%n    p25=%s%n    p50=%s%n    p75=%s%n    p95=%s%n    p99=%s%n    max=%s%n",
                        tag,
                        durations.size(),
                        durations.get(0),
                        durations.get(Math.min(size - 1, size / 4)),
                        durations.get(Math.min(size - 1, size / 2)),
                        durations.get(Math.min(size - 1, size - size / 4)),
                        durations.get(Math.min(size - 1, size - size / 20)),
                        durations.get(Math.min(size - 1, size - size / 100)),
                        durations.get(size - 1));
            });
            System.err.println();
            System.err.println();
        }
    }
}
