package com.example.jbfs;

import javax.annotation.ParametersAreNonnullByDefault;

import com.google.common.base.Preconditions;

@ParametersAreNonnullByDefault
class ChunkListBitSet implements FsBitSet {
    static final int BUFFER_SIZE = 64;
    private static final int[] BITS = new int[]{
            1,
            1 << 1,
            1 << 2,
            1 << 3,
            1 << 4,
            1 << 5,
            1 << 6,
            1 << 7,
    };
    private static final int[] REVERSED_MASKS = new int[]{
            ~(BITS[0] - 1) & 0xff,
            ~(BITS[1] - 1) & 0xff,
            ~(BITS[2] - 1) & 0xff,
            ~(BITS[3] - 1) & 0xff,
            ~(BITS[4] - 1) & 0xff,
            ~(BITS[5] - 1) & 0xff,
            ~(BITS[6] - 1) & 0xff,
            ~(BITS[7] - 1) & 0xff,
    };
    private final ChunkList chunkList;
    private final ChunkList.Mark chunkListStartMark;
    private ChunkList.Mark bufferStartMark;
    private byte[] buffer;
    private long bufferStart;

    ChunkListBitSet(ChunkList chunkList) {
        this.chunkList = chunkList;
        chunkListStartMark = chunkList.mark();
        bufferStartMark = chunkList.mark();
        buffer = new byte[0];
        bufferStart = 0;
    }

    /**
     * Increase size of bitset.
     *
     * @param count How many bytes to allocate.
     */
    public void expand(int count) {
        try (ChunkList.Mark ignored = chunkList.mark()) {
            chunkList.forwardToEnd();
            chunkList.expand(count);
        }
    }

    @Override
    public boolean get(long atBit) {
        long atByte = atBit >> 3;
        atBit &= 7;
        Preconditions.checkState(prepareBufferIfPossible(atByte));
        return (buffer[(int) (atByte - bufferStart)] & BITS[(int) atBit]) != 0;
    }

    @Override
    public void set(long atBit, boolean value) {
        long atByte = atBit >> 3;
        atBit &= 7;
        Preconditions.checkState(prepareBufferIfPossible(atByte));
        int bufferOffset = (int) (atByte - bufferStart);
        if (value) {
            buffer[bufferOffset] |= BITS[(int) atBit];
        } else {
            buffer[bufferOffset] &= ~BITS[(int) atBit];
        }
        bufferStartMark.close();
        chunkList.forwardStrict(bufferOffset);
        chunkList.replaceByte(buffer[bufferOffset]);
    }

    @Override
    public long nextSetBit(long fromBit) {
        long atByte = fromBit >> 3;
        fromBit &= 7;
        while (prepareBufferIfPossible(atByte)) {
            byte valueAsByte = buffer[(int) (atByte - bufferStart)];
            int value = valueAsByte & REVERSED_MASKS[(int) fromBit];
            if (value != 0) {
                return (atByte << 3) + Integer.numberOfTrailingZeros(value);
            }
            fromBit = 0;
            ++atByte;
        }
        return -1;
    }

    @Override
    public long nextClearBit(long fromBit) {
        long atByte = fromBit >> 3;
        fromBit &= 7;
        while (prepareBufferIfPossible(atByte)) {
            byte valueAsByte = buffer[(int) (atByte - bufferStart)];
            int value =(~valueAsByte) & REVERSED_MASKS[(int) fromBit];
            if (value != 0) {
                return (atByte << 3) + Integer.numberOfTrailingZeros(value);
            }
            fromBit = 0;
            ++atByte;
        }
        return -1;
    }

    private boolean prepareBufferIfPossible(long atByte) {
        boolean beforeBuffer = atByte < bufferStart;
        boolean afterBuffer = atByte >= bufferStart + buffer.length;
        if (!beforeBuffer && !afterBuffer) {
            return true;
        }
        if (beforeBuffer) {
            chunkListStartMark.close();
            bufferStart = 0;
        } else {
            bufferStartMark.close();
        }
        long desiredCount = atByte - bufferStart;
        long readCount = chunkList.forward(desiredCount);
        bufferStart += readCount;
        bufferStartMark = chunkList.mark();
        if (readCount != desiredCount) {
            buffer = new byte[0];
        } else {
            buffer = chunkList.readBytes(BUFFER_SIZE);
        }
        return buffer.length > 0;
    }
}
