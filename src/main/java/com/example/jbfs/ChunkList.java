package com.example.jbfs;

import java.io.Closeable;
import java.nio.ByteBuffer;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.annotation.concurrent.NotThreadSafe;

import com.google.common.base.Preconditions;
import com.google.common.primitives.UnsignedBytes;
import com.google.common.primitives.UnsignedLong;

/**
 * Implementation of double-linked array list that uses {@link ChunkListStorage} as a backend.
 * <p>
 * Provides kind of iterator from start to end, allows to remember current position and restore it later, allows to
 * insert some data or remove some data at some place.
 * <p>
 * Binary interface of each chunk:
 * <p>
 * <table>
 * <tr><th>Byte positions</th><th>Purpose</th></tr>
 * <tr><td>[0-8)</td><td>Offset to the next chunk or zero if this is the last chunk in list.</td></tr>
 * <tr><td>[8, 16)</td><td>Offset to the previous chunk or zero if this is the first chunk in list.</td></tr>
 * <tr><td>[16, 20)</td><td>Size of data that current chunk contains.</td></tr>
 * <tr><td>[20, 20 + current chunk data size)</td><td>Data.</td></tr>
 * </table>
 * <p>
 * All integers are stored in big-endian format.
 */
@NotThreadSafe
class ChunkList {
    private final ChunkListStorage storageBackend;
    private final Allocator allocator;

    ChunkList(FileChannelAdaptor fileChannel, Allocator allocator, long startPosition) {
        this(
                new FileChunkListStorage(fileChannel),
                allocator,
                startPosition);
    }

    private ChunkList(ChunkListStorage storageBackend, Allocator allocator, long startPosition) {
        this.storageBackend = storageBackend;
        this.allocator = allocator;
        Preconditions.checkArgument(allocator.getBlockSize() > ChunkListStorage.METADATA_SIZE,
                "Too small block size");
        storageBackend.jumpToChunk(startPosition);
    }

    /**
     * Checks if positioned at chunk list end. This call always changes position to the next chunk if positioned
     * after the last byte of chunk.
     *
     * @return true if positioned at chunk list end.
     */
    boolean flipToChunkStartAndCheckIsListEnd() {
        if (storageBackend.getPosition() == storageBackend.getCurrentChunkDataEnd()) {
            if (storageBackend.getNextChunkOffset() == 0) {
                return true;
            }
            assert storageBackend.getCurrentChunkDataSize() != 0;
            storageBackend.jumpToChunk(storageBackend.getNextChunkOffset());
        }
        return false;
    }

    /**
     * Checks if positioned at chunk list start. This call always changes position to the end of previous chunk
     * if positioned at the first byte of chunk.
     *
     * @return true if positioned at chunk list start.
     */
    boolean flipToChunkEndAndCheckIsListStart() {
        if (storageBackend.getPosition() == storageBackend.getCurrentChunkDataStart()) {
            if (storageBackend.getPreviousChunkOffset() == 0) {
                return true;
            }
            assert storageBackend.getCurrentChunkDataSize() != 0;
            storageBackend.jumpToChunk(storageBackend.getPreviousChunkOffset());
        }
        return false;
    }

    /**
     * Reads one byte from current position and increments current position.
     *
     * @throws NoSuchElementException If positioned at chunk list end.
     */
    byte readByte() {
        if (flipToChunkStartAndCheckIsListEnd()) {
            throw new NoSuchElementException();
        }
        return storageBackend.readByte();
    }

    /**
     * Reads specified count byte from current position and increments current position for the specified count.
     *
     * @param size How many bytes to read
     * @throws NoSuchElementException If positioned at chunk list end.
     */
    byte[] readBytesStrict(int size) {
        byte[] result = readBytes(size);
        Preconditions.checkState(result.length == size);
        return result;
    }

    /**
     * Reads at most `size` bytes into array from current position.
     *
     * @param size Count of bytes.
     * @return Array with size not less than zero and not greater than size.
     */
    byte[] readBytes(int size) {
        byte[] result = new byte[size];
        int offset = 0;
        while (size > 0 && !flipToChunkStartAndCheckIsListEnd()) {
            int parcelSize =
                    Math.min(size, (int) (storageBackend.getCurrentChunkDataEnd() - storageBackend.getPosition()));
            parcelSize = storageBackend.readBytes(result, offset, parcelSize);
            Preconditions.checkState(parcelSize > 0);
            offset += parcelSize;
            size -= parcelSize;
        }
        if (offset != result.length) {
            result = Arrays.copyOf(result, offset);
        }
        return result;
    }

    /**
     * Read bytes from current position until current chunk end.
     */
    byte[] readBytesUntilChunkEnd() {
        return readBytesStrict((int) (storageBackend.getCurrentChunkDataEnd() - storageBackend.getPosition()));
    }

    /**
     * Reads integral numbers from current position.
     *
     * @param types Classes of requested objects. Possible classes are: Byte, Integer, Long.
     * @return Array of read objects.
     */
    Object[] readObjects(Class... types) {
        Object[] result = new Object[types.length];
        int bufferSize = 0;
        for (Class type : types) {
            if (type == Byte.class) {
                ++bufferSize;
            } else if (type == Integer.class) {
                bufferSize += 4;
            } else if (type == Long.class) {
                bufferSize += 8;
            } else {
                throw new IllegalStateException("Unsupported type " + type);
            }
        }
        ByteBuffer temporaryStorage = ByteBuffer.wrap(readBytesStrict(bufferSize));
        temporaryStorage.position(0);
        int i = 0;
        for (Class type : types) {
            if (type == Byte.class) {
                result[i] = temporaryStorage.get();
            } else if (type == Integer.class) {
                result[i] = temporaryStorage.getInt();
            } else if (type == Long.class) {
                result[i] = temporaryStorage.getLong();
            } else {
                throw new IllegalStateException("Unsupported type " + type);
            }
            ++i;
        }
        assert temporaryStorage.position() == bufferSize;
        return result;
    }

    /**
     * Writes one byte at current position and increments current position.
     *
     * @param data New byte
     * @throws NoSuchElementException If positioned at chunk list end.
     */
    void replaceByte(byte data) {
        if (flipToChunkStartAndCheckIsListEnd()) {
            throw new IndexOutOfBoundsException();
        }
        storageBackend.writeByte(data);
    }

    /**
     * Writes byte from array at current position and increments current position to the array size.
     *
     * @param data New bytes.
     * @throws NoSuchElementException If not enough space in chunk list to write all bytes from initial position.
     */
    void replaceBytes(byte[] data) {
        if (replaceBytes(data, 0, data.length) != data.length) {
            throw new IndexOutOfBoundsException();
        }
    }

    /**
     * Writes byte from array at current position and increments current position to the array size.
     *
     * @param data   New bytes.
     * @param offset Offset from start of `data`.
     * @param length Count of bytes from offset.
     * @throws NoSuchElementException If not enough space in chunk list to write all bytes from initial position.
     */
    int replaceBytes(byte[] data, int offset, int length) {
        int count = 0;
        while (length > 0) {
            int packetLength =
                    Math.min(length, (int) (storageBackend.getCurrentChunkDataEnd() - storageBackend.getPosition()));
            storageBackend.writeBytes(data, offset, packetLength);
            offset += packetLength;
            length -= packetLength;
            count += packetLength;
            if (flipToChunkStartAndCheckIsListEnd()) {
                break;
            }
        }
        return count;
    }

    /**
     * Skips specified count of bytes.
     *
     * @param count Count of bytes.
     * @throws IllegalStateException If reached chunk list end before specified count of bytes was skipped.
     */
    void forwardStrict(long count) {
        long realCount = forward(count);
        Preconditions.checkState(count == realCount, "Expected %s, got %s", count, realCount);
    }

    /**
     * Skips specified count of bytes.
     *
     * @param count Count of bytes.
     * @return Really skipped count of bytes. It may be less than specified count if reached chunk list.
     */
    long forward(long count) {
        if (count == 0) {
            return 0;
        }
        Preconditions.checkArgument(count > 0);
        long initialCount = count;
        while (true) {
            long betweenPositionAndChunkEnd = storageBackend.getCurrentChunkDataEnd() - storageBackend.getPosition();
            if (count < betweenPositionAndChunkEnd) {
                storageBackend.jumpToPosition(storageBackend.getPosition() + count);
                return initialCount;
            }
            count -= betweenPositionAndChunkEnd;
            storageBackend.jumpToPosition(storageBackend.getCurrentChunkDataEnd());
            if (flipToChunkStartAndCheckIsListEnd()) {
                return initialCount - count;
            }
        }
    }

    /**
     * Skips specified count of bytes backward. Opposite of {@link #forward(long)}.
     *
     * @param count Count of bytes.
     * @return Really skipped count of bytes. It may be less than specified count if reached chunk list.
     */
    long rewind(long count) {
        if (count == 0) {
            return 0;
        }
        Preconditions.checkArgument(count > 0);
        long initialCount = count;
        while (true) {
            long betweenChunkStartAndPosition =
                    storageBackend.getPosition() - storageBackend.getCurrentChunkDataStart();
            if (count <= betweenChunkStartAndPosition) {
                storageBackend.jumpToPosition(storageBackend.getPosition() - count);
                return initialCount;
            }
            count -= betweenChunkStartAndPosition;
            storageBackend.jumpToChunk(storageBackend.getPreviousChunkOffset());
            storageBackend.jumpToPosition(storageBackend.getCurrentChunkDataEnd());
            if (flipToChunkEndAndCheckIsListStart()) {
                return initialCount - count;
            }
        }
    }

    /**
     * Skips specified count of bytes backward. Opposite of {@link #forwardStrict(long)}.
     *
     * @param count Count of bytes.
     * @throws IllegalStateException If reached chunk list start before specified count of bytes was skipped.
     */
    void rewindStrict(long count) {
        long realCount = rewind(count);
        Preconditions.checkState(count == realCount, "Expected %s, got %s", count, realCount);
    }

    /**
     * Sets position to end of the chunk list.
     */
    void forwardToEnd() {
        while (!flipToChunkStartAndCheckIsListEnd()) {
            storageBackend.jumpToPosition(storageBackend.getCurrentChunkDataEnd());
        }
    }

    /**
     * Adds free space at the current position. May acquire new blocks using allocator and insert them between current
     * and next blocks.
     *
     * @param diff Size in bytes
     */
    void expand(int diff) {
        int maxDataSize = allocator.getBlockSize() - ChunkListStorage.METADATA_SIZE;
        byte[] dataToMove = new byte[(int) (storageBackend.getCurrentChunkDataEnd() - storageBackend.getPosition())];
        try (Mark ignored = mark()) {
            storageBackend.readBytesStrict(dataToMove);
            int[] chunkSizes = new int[diff / maxDataSize + 2];
            int chunkSizeIdx = 0;
            diff += storageBackend.getCurrentChunkDataSize();
            while (diff >= maxDataSize) {
                chunkSizes[chunkSizeIdx++] = maxDataSize;
                diff -= maxDataSize;
            }
            if (diff > 0) {
                chunkSizes[chunkSizeIdx++] = diff;
            }

            addChunks(chunkSizes, chunkSizeIdx);
            rewindStrict(dataToMove.length);
            replaceBytes(dataToMove);
        }
    }

    private void addChunks(int[] chunkSizeQueue, final int count) {
        long[] positionQueue = new long[count];
        long[] nextChunkQueue = new long[count];
        long[] previousChunkQueue = new long[count];
        long[] allocated = new long[count];
        long lastNext = storageBackend.getNextChunkOffset();
        long previousOfCurrent = storageBackend.getPreviousChunkOffset();
        long currentChunk = storageBackend.getChunkStartPosition();

        int idx = 0;
        int allocates = 0;
        while (true) {
            positionQueue[idx] = currentChunk;
            nextChunkQueue[idx] = lastNext;
            previousChunkQueue[idx] = previousOfCurrent;
            if (idx + 1 == count) {
                break;
            }
            previousOfCurrent = currentChunk;
            try {
                currentChunk = allocator.allocate();
            } catch (RuntimeException exc) {
                while (allocates > 0) {
                    // if error will be thrown here, that error will be fatal
                    allocator.deallocate(allocated[--allocates]);
                }
                throw exc;
            }
            allocated[allocates++] = currentChunk;
            nextChunkQueue[idx] = currentChunk;
            ++idx;
        }
        for (idx = 0; idx < count; ++idx) {
            storageBackend.writeNextChunkOffsetFor(positionQueue[idx], nextChunkQueue[idx]);
            storageBackend.writePreviousChunkPositionFor(positionQueue[idx], previousChunkQueue[idx]);
            storageBackend.writeCurrentChunkDataSizeFor(positionQueue[idx], chunkSizeQueue[idx]);
        }
        storageBackend.jumpToChunk(positionQueue[count - 1]);
        if (storageBackend.getNextChunkOffset() != 0) {
            storageBackend.writePreviousChunkPositionFor(storageBackend.getNextChunkOffset(), positionQueue[count - 1]);
        }
        storageBackend.jumpToPosition(storageBackend.getCurrentChunkDataEnd());
    }

    /**
     * Cuts off data from current position. May return some blocks to allocator.
     *
     * @param diff Size in bytes.
     * @throws NoSuchElementException If can't cut of specified count of bytes because suddenly reached end of list.
     */
    void shrink(int diff) {
        Preconditions.checkArgument(diff > 0);
        long initialPreviousChunkOffset = storageBackend.getPreviousChunkOffset();

        boolean startingChunkShouldBeRemoved = false;
        ChunkList.Mark mark = mark();
        if (atChunkListStart()) {
            diff = moveLastMatchingChunkAndFreeOthers(diff);
        } else {
            // 1. shrink in current chunk from here to end of chunk
            // 2. move to next chunk
            Map.Entry<Integer, Boolean> firstPartResult = shrinkFromPositionUntilChunkEnd(diff);
            diff = firstPartResult.getKey();
            startingChunkShouldBeRemoved = firstPartResult.getValue();
            // 3. cut off chunk
            // 4. repeat 2-3
            // 5. move to next chunk
            diff = shrinkSeveralChunks(diff);
        }
        // 6. shrink from start to desired position
        shrinkFromStart(diff);
        if (startingChunkShouldBeRemoved) {
            assert initialPreviousChunkOffset != 0;
            storageBackend.jumpToChunk(initialPreviousChunkOffset);
            // jumpToChunk re-reads whole header, so previous call to jumpToChunk
            // sets correct next chunk offset.
            if (storageBackend.getNextChunkOffset() != 0) {
                storageBackend.jumpToChunk(storageBackend.getNextChunkOffset());
            }
        } else {
            mark.close();
        }
    }

    /**
     * The first chunk can't be returned to allocator because there may be entities that refers to the start of chunk
     * list. This procedure finds first chunk that will not be removed and copies its whole content to the current
     * chunk, and finally frees those chunk.
     */
    private int moveLastMatchingChunkAndFreeOthers(int diff) {
        if (diff > storageBackend.getCurrentChunkDataSize()) {
            long destination = storageBackend.getChunkStartPosition();
            long initialPreviousChunkOffset = storageBackend.getPreviousChunkOffset();
            List<Long> chunksToFree = new ArrayList<>();
            long source;
            do {
                diff -= storageBackend.getCurrentChunkDataSize();
                source = storageBackend.getNextChunkOffset();
                if (source == 0) {
                    break;
                }
                chunksToFree.add(source);
                storageBackend.jumpToChunk(source);
            } while (diff >= storageBackend.getCurrentChunkDataSize());

            if (source == 0) {
                // shrunk whole chunk list
                storageBackend.jumpToPosition(destination);
                storageBackend.clearBytes(allocator.getBlockSize());
            } else {
                assert source != destination;
                byte[] data = new byte[allocator.getBlockSize()];
                storageBackend.jumpToPosition(source);
                storageBackend.readBytesStrict(data);
                storageBackend.jumpToPosition(destination);
                storageBackend.writeBytes(data);
                storageBackend.writePreviousChunkPositionFor(destination, initialPreviousChunkOffset);
            }

            for (long offset : chunksToFree) {
                storageBackend.jumpToPosition(offset);
                storageBackend.clearBytes(allocator.getBlockSize());
                allocator.deallocate(offset);
            }

            storageBackend.jumpToChunk(destination);
            if (storageBackend.getNextChunkOffset() != 0) {
                storageBackend.writePreviousChunkPositionFor(storageBackend.getNextChunkOffset(), destination);
            }
        }
        return diff;
    }

    /**
     * Unlike {@link #flipToChunkEndAndCheckIsListStart()} does not jump between chunks.
     */
    private boolean atChunkListStart() {
        return storageBackend.getPreviousChunkOffset() == 0
                && storageBackend.getPosition() == storageBackend.getCurrentChunkDataStart();
    }

    /**
     * Cuts off some data from current position and inside one chunk.
     *
     * @param diff How many bytes to cut off.
     */
    private void shrinkFromStart(int diff) {
        if (diff > 0) {
            long repeats = storageBackend.getCurrentChunkDataEnd() - storageBackend.getPosition() - (long) diff;
            assert repeats >= 0;
            long oldPosition = storageBackend.getPosition();
            storageBackend.jumpToPosition(storageBackend.getPosition() + diff);
            byte[] data = new byte[(int) repeats];
            storageBackend.readBytesStrict(data);
            storageBackend.jumpToPosition(oldPosition);
            storageBackend.writeBytes(data);
            writeZerosUntilChunkEnd();
            storageBackend.decreaseCurrentChunkDataSize(diff);
        }
    }

    /**
     * Cuts off bytes between current position and the end of the current chunk. Can't cut off more bytes than the
     * current chunk contains.
     * <p>
     * Should not be called at start of chunk list.
     *
     * @param diff How many bytes should be cut off.
     * @return Remained count of bytes to cut off and flag that starting chunk will be removed.
     */
    private Map.Entry<Integer, Boolean> shrinkFromPositionUntilChunkEnd(int diff) {
        long initialPosition = storageBackend.getPosition();
        long count = storageBackend.getCurrentChunkDataEnd() - initialPosition;
        boolean startingChunkWillBeRemoved = false;
        if (diff >= count) {
            if (initialPosition == storageBackend.getCurrentChunkDataStart()) {
                Preconditions.checkState(storageBackend.getPreviousChunkOffset() != 0,
                        "Bug. That case must be handled in other procedure.");
                startingChunkWillBeRemoved = true;
            } else {
                writeZerosUntilChunkEnd();
                storageBackend.decreaseCurrentChunkDataSize((int) count);
                diff -= count;
                if (diff == 0) {
                    storageBackend.jumpToPosition(initialPosition);
                } else {
                    Preconditions.checkState(storageBackend.getNextChunkOffset() != 0);
                    storageBackend.jumpToChunk(storageBackend.getNextChunkOffset());
                }
            }
        }
        return new AbstractMap.SimpleEntry<>(diff, startingChunkWillBeRemoved);
    }

    /**
     * Clears chunks and returns their blocks to allocator. Starts clearing from the current chunk.
     * <p>
     * Should not be called at start of chunk list.
     *
     * @param diff How many bytes should be cut off.
     * @return Count of bytes that was not cut of because reached the chunk that should not be returned to allocator.
     * Those bytes should be cut inside of that chunk.
     */
    private int shrinkSeveralChunks(int diff) {
        while (diff >= storageBackend.getCurrentChunkDataSize() && diff > 0) {
            diff -= storageBackend.getCurrentChunkDataSize();
            if (diff == 0) {
                long deletingChunkStart = storageBackend.getChunkStartPosition();
                long realPreviousChunk = storageBackend.getPreviousChunkOffset();
                storageBackend.writeNextChunkOffsetFor(realPreviousChunk, storageBackend.getNextChunkOffset());
                storageBackend.jumpToPosition(deletingChunkStart);
                storageBackend.clearBytes(allocator.getBlockSize());  // should not be moved to allocator
                allocator.deallocate(storageBackend.getChunkStartPosition());
                storageBackend.jumpToChunk(realPreviousChunk);
                if (storageBackend.getNextChunkOffset() == 0) {
                    storageBackend.jumpToPosition(storageBackend.getCurrentChunkDataEnd());
                } else {
                    storageBackend
                            .writePreviousChunkPositionFor(storageBackend.getNextChunkOffset(), realPreviousChunk);
                    storageBackend.jumpToChunk(storageBackend.getNextChunkOffset());
                }
            } else {
                Preconditions.checkState(storageBackend.getNextChunkOffset() != 0);
                storageBackend.writePreviousChunkPositionFor(storageBackend.getNextChunkOffset(),
                        storageBackend.getPreviousChunkOffset());
                storageBackend.writeNextChunkOffsetFor(storageBackend.getPreviousChunkOffset(),
                        storageBackend.getNextChunkOffset());
                storageBackend.jumpToPosition(storageBackend.getChunkStartPosition());
                storageBackend.clearBytes(allocator.getBlockSize());
                allocator.deallocate(storageBackend.getChunkStartPosition());
                storageBackend.jumpToChunk(storageBackend.getNextChunkOffset());
            }
        }
        return diff;
    }

    private void writeZerosUntilChunkEnd() {
        long count = storageBackend.getCurrentChunkDataEnd() - storageBackend.getPosition();
        storageBackend.clearBytes((int) count);
    }

    /**
     * Returns all blocks to allocator from current chunk to end of the list.
     */
    void deallocate() {
        long chunkStart = storageBackend.getChunkStartPosition();
        do {
            storageBackend.jumpToChunk(chunkStart);
            long nextOffset = storageBackend.getNextChunkOffset();
            allocator.deallocate(chunkStart);
            chunkStart = nextOffset;
        } while (chunkStart != 0);
    }

    /**
     * Remembers current position in chunk allowing to restore this position later.
     * <p>
     * <b>Note</b> that returned mark may become invalid if pointed chunk will be cut off by {@link #shrink(int)}.
     *
     * @return Special object that used to restore position.
     */
    Mark mark() {
        return new Mark();
    }

    /**
     * Reads from current position 8-bytes integer in big-endian format.
     */
    long readLong() {
        byte[] data = readBytesStrict(8);
        return ((long) UnsignedBytes.toInt(data[0]) << 56)
                | ((long) UnsignedBytes.toInt(data[1]) << 48)
                | ((long) UnsignedBytes.toInt(data[2]) << 40)
                | ((long) UnsignedBytes.toInt(data[3]) << 32)
                | ((long) UnsignedBytes.toInt(data[4]) << 24)
                | ((long) UnsignedBytes.toInt(data[5]) << 16)
                | ((long) UnsignedBytes.toInt(data[6]) << 8)
                | (long) UnsignedBytes.toInt(data[7]);
    }

    /**
     * Writes at current position 8-bytes integer in big-endian format.
     *
     * @param data New data to write.
     */
    void replaceLong(long data) {
        byte[] bytes = new byte[8];
        bytes[0] = UnsignedLong.fromLongBits(data).dividedBy(UnsignedLong.valueOf(1L << 56)).byteValue();
        bytes[1] = (byte) (data >> 48);
        bytes[2] = (byte) (data >> 40);
        bytes[3] = (byte) (data >> 32);
        bytes[4] = (byte) (data >> 24);
        bytes[5] = (byte) (data >> 16);
        bytes[6] = (byte) (data >> 8);
        bytes[7] = (byte) data;
        replaceBytes(bytes);
    }

    /**
     * Special object that allows to restore position.
     */
    class Mark implements Closeable {
        private final long position;
        private final long currentChunkStart;

        private Mark() {
            this.position = storageBackend.getPosition();
            this.currentChunkStart = storageBackend.getChunkStartPosition();
        }

        /**
         * Used to set position in other instance of {@link ChunkList} than the list where mark was created.
         */
        void restore(ChunkList chunkList) {
            chunkList.storageBackend.jumpToChunk(currentChunkStart);
            chunkList.storageBackend.jumpToPosition(position);
        }

        /**
         * Restores position in the chunk list.
         */
        @Override
        public void close() {
            restore(ChunkList.this);
        }
    }
}
