package com.example.jbfs;

/**
 * Info about some file or directory.
 */
public class DirectoryChildInfo {
    private final String name;
    private final boolean directory;
    private final long contentOffset;
    private final ChunkList.Mark nodeMark;

    DirectoryChildInfo(String name, boolean directory, long contentOffset, ChunkList.Mark nodeMark) {
        this.name = name;
        this.directory = directory;
        this.contentOffset = contentOffset;
        this.nodeMark = nodeMark;
    }

    /** Name of file or directory. */
    public String getName() {
        return name;
    }

    public boolean isDirectory() {
        return directory;
    }

    public boolean isFile() {
        return !directory;
    }

    /**
     * Offset from start of real file to start of node's content.
     * Every node uses {@link ChunkList} to store node's content.
     */
    long getContentOffset() {
        return contentOffset;
    }

    /**
     * Object to easily jump to start of node's content.
     */
    ChunkList.Mark getNodeMark() {
        return nodeMark;
    }
}
