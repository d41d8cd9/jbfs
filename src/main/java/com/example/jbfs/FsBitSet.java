package com.example.jbfs;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public interface FsBitSet {
    /**
     * Gets value of the specified bit.
     */
    boolean get(long atBit);

    /**
     * Sets value of the specified bit.
     */
    void set(long atBit, boolean value);

    /**
     * Finds position of the first non-set bit, starting from the specified bit.
     *
     * @param fromBit Start seeking from this bit, including specified position.
     */
    long nextClearBit(long fromBit);

    /**
     * Finds position of the first set bit, starting from the specified bit.
     *
     * @param fromBit Start seeking from this bit, including specified position.
     */
    long nextSetBit(long fromBit);
}
