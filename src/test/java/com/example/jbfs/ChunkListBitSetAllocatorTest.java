package com.example.jbfs;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayDeque;
import java.util.Deque;

import javax.annotation.ParametersAreNonnullByDefault;

import com.example.jbfs.exceptions.NoSpaceLeft;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

@ParametersAreNonnullByDefault
public class ChunkListBitSetAllocatorTest {
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void allocateAndDeallocate() throws IOException {
        int blockSize = 32;
        int fsSize = blockSize * 2049;
        Path backendFile = temporaryFolder.newFile().toPath();
        try (FileChannel channel = FileChannel.open(backendFile,
                StandardOpenOption.READ, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING))
        {
            channel.truncate(fsSize);
            channel.position(fsSize - 1);
            channel.write(ByteBuffer.wrap(new byte[]{0}));
            FileChannelAdaptor adaptor = new FileChannelAdaptor(channel);
            ChunkListBitSetAllocator.format(adaptor, 0, blockSize, blockSize);

            ChunkListBitSetAllocator allocator = new ChunkListBitSetAllocator(adaptor, 0, blockSize, blockSize, fsSize);
            Deque<Long> allocates = new ArrayDeque<>();
            Assertions.assertThatCode(
                    () -> {
                        for (int i = 0; i < fsSize; ++i) {
                            allocates.addFirst(allocator.allocate());
                        }
                    })
                    .describedAs("Allocate as much blocks as possible. Should reach file end.")
                    .isInstanceOf(NoSpaceLeft.class);
            Assertions.assertThat(allocates)
                    .describedAs("Allocator should not acquire one block twice")
                    .doesNotHaveDuplicates();
            Assertions.assertThatCode(
                    () -> {
                        for (long blockStart : allocates) {
                            ChunkList chunkList = new ChunkList(adaptor, allocator, blockStart);
                            assert chunkList.flipToChunkStartAndCheckIsListEnd();  // every allocated block in fresh filesystem should be empty
                            chunkList.expand(blockSize - ChunkListStorage.METADATA_SIZE);
                            for (int i = 0; i < blockSize - ChunkListStorage.METADATA_SIZE; ++i) {
                                chunkList.replaceByte((byte) 'x');
                            }
                        }
                    })
                    .describedAs("Every allocated block should be writable.")
                    .doesNotThrowAnyException();
            int count = allocates.size();
            Assertions.assertThatCode(
                    () -> {
                        while (!allocates.isEmpty()) {
                            allocator.deallocate(allocates.pollFirst());
                        }
                    })
                    .doesNotThrowAnyException();

            for (int attempt = 1; attempt <= 3; ++attempt) {
                String description = String.format("Attempt %d", attempt);
                Assertions.assertThatCode(
                        () -> {
                            for (int i = 0; i < count; ++i) {
                                allocates.push(allocator.allocate());
                            }
                        })
                        .describedAs(description)
                        .doesNotThrowAnyException();
                Assertions.assertThatCode(allocator::allocate)
                        .describedAs(description)
                        .isInstanceOf(NoSpaceLeft.class);
                boolean reverse = attempt % 2 == 0;
                Assertions.assertThatCode(
                        () -> {
                            while (!allocates.isEmpty()) {
                                allocator.deallocate(reverse ? allocates.pollFirst() : allocates.pollLast());
                            }
                        })
                        .describedAs(description)
                        .doesNotThrowAnyException();
            }
        }
    }
}