package com.example.jbfs;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.annotation.ParametersAreNonnullByDefault;

import com.example.jbfs.exceptions.NoSuchFileOrDirectory;
import com.google.common.base.Charsets;
import org.apache.commons.io.IOUtils;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.JUnitSoftAssertions;
import org.assertj.core.groups.Tuple;
import org.junit.After;
import org.junit.AssumptionViolatedException;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

@ParametersAreNonnullByDefault
public class FileSystemTest {
    @ClassRule
    public static final TemporaryFolder temporaryFolder = new TemporaryFolder();
    private static final int BLOCK_SIZE = 32;
    private static final long NEW_FS_SIZE = BLOCK_SIZE * 96;  // Some size enough for whole test suite.
    @Rule
    public final JUnitSoftAssertions softly = new JUnitSoftAssertions();
    private FileSystem fileSystem;

    private static String readLine(InputStream stream) throws IOException {
        StringBuilder builder = new StringBuilder();
        int chr;
        while ((chr = stream.read()) >= 0 && chr != '\n') {
            builder.append((char) chr);
        }
        return builder.toString();
    }

    @Before
    public void setUp() throws IOException {
        Path backend = temporaryFolder.newFile().toPath();
        FileBackedFileSystem.format(backend, NEW_FS_SIZE, BLOCK_SIZE);
        this.fileSystem = FileBackedFileSystem.mount(backend);
    }

    @After
    public void tearDown() {
        try {
            if (fileSystem != null) {
                ((FileBackedFileSystem) fileSystem).close();
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Test
    public void emptyListDir() throws InterruptedException {
        softly.assertThat(fileSystem.listDirectory("/")).isEmpty();
    }

    @Test
    public void makeDirectoryInRoot() throws InterruptedException {
        fileSystem.makeDirectory("/foo");
        softly.assertThat(fileSystem.listDirectory("/"))
                .extracting("name", "directory")
                .containsExactlyInAnyOrder(Tuple.tuple("foo", true));

        fileSystem.makeDirectory("/bar/");
        softly.assertThat(fileSystem.listDirectory("/"))
                .extracting("name", "directory")
                .containsExactlyInAnyOrder(
                        Tuple.tuple("foo", true),
                        Tuple.tuple("bar", true));

        fileSystem.makeDirectory("////baz//");
        softly.assertThat(fileSystem.listDirectory("/"))
                .extracting("name", "directory")
                .containsExactlyInAnyOrder(
                        Tuple.tuple("foo", true),
                        Tuple.tuple("bar", true),
                        Tuple.tuple("baz", true));
    }

    @Test
    public void makeNestedDirectories() throws InterruptedException {
        fileSystem.makeDirectory("/foo");
        fileSystem.makeDirectory("/hurr");

        fileSystem.makeDirectory("/foo/bar");
        softly.assertThat(fileSystem.listDirectory("/"))
                .describedAs("mkdir /foo/bar; ls /")
                .extracting("name", "directory")
                .containsExactlyInAnyOrder(
                        Tuple.tuple("foo", true),
                        Tuple.tuple("hurr", true));
        softly.assertThat(fileSystem.listDirectory("/foo"))
                .describedAs("mkdir /foo/bar; ls /foo")
                .extracting("name", "directory")
                .containsExactlyInAnyOrder(
                        Tuple.tuple("bar", true));
        softly.assertThat(fileSystem.listDirectory("/foo/bar"))
                .describedAs("mkdir /foo/bar; ls /foo/bar")
                .isEmpty();
        softly.assertThat(fileSystem.listDirectory("/hurr"))
                .describedAs("mkdir /foo/bar; ls /hurr")
                .isEmpty();

        fileSystem.makeDirectory("/foo/bar/baz");
        softly.assertThat(fileSystem.listDirectory("/"))
                .describedAs("mkdir /foo/bar/baz; ls /")
                .extracting("name", "directory")
                .containsExactlyInAnyOrder(
                        Tuple.tuple("foo", true),
                        Tuple.tuple("hurr", true));
        softly.assertThat(fileSystem.listDirectory("/foo"))
                .describedAs("mkdir /foo/bar/baz; ls /foo")
                .extracting("name", "directory")
                .containsExactlyInAnyOrder(
                        Tuple.tuple("bar", true));
        softly.assertThat(fileSystem.listDirectory("/foo/bar"))
                .describedAs("mkdir /foo/bar/baz; ls /foo/bar")
                .extracting("name", "directory")
                .containsExactlyInAnyOrder(
                        Tuple.tuple("baz", true));
        softly.assertThat(fileSystem.listDirectory("/foo/bar/baz"))
                .describedAs("mkdir /foo/bar/baz; ls /foo/bar/baz")
                .isEmpty();
        softly.assertThat(fileSystem.listDirectory("/hurr"))
                .describedAs("mkdir /foo/bar/baz; ls /hurr")
                .isEmpty();

        fileSystem.makeDirectory("/foo/bee");
        softly.assertThat(fileSystem.listDirectory("/"))
                .describedAs("mkdir /foo/bee; ls /")
                .extracting("name", "directory")
                .containsExactlyInAnyOrder(
                        Tuple.tuple("foo", true),
                        Tuple.tuple("hurr", true));
        softly.assertThat(fileSystem.listDirectory("/foo"))
                .describedAs("mkdir /foo/bee; ls /foo")
                .extracting("name", "directory")
                .containsExactlyInAnyOrder(
                        Tuple.tuple("bar", true),
                        Tuple.tuple("bee", true));
        softly.assertThat(fileSystem.listDirectory("/foo/bar"))
                .describedAs("mkdir /foo/bee; ls /foo/bar")
                .extracting("name", "directory")
                .containsExactlyInAnyOrder(
                        Tuple.tuple("baz", true));
        softly.assertThat(fileSystem.listDirectory("/foo/bar/baz"))
                .describedAs("mkdir /foo/bee; ls /foo/bar/baz")
                .isEmpty();
        softly.assertThat(fileSystem.listDirectory("/foo/bee"))
                .describedAs("mkdir /foo/bee; ls /foo/bee")
                .isEmpty();
        softly.assertThat(fileSystem.listDirectory("/hurr"))
                .describedAs("mkdir /foo/bee; ls /hurr")
                .isEmpty();

        fileSystem.makeDirectory("/hurr/durr");
        softly.assertThat(fileSystem.listDirectory("/"))
                .describedAs("mkdir /hurr/durr; ls /")
                .extracting("name", "directory")
                .containsExactlyInAnyOrder(
                        Tuple.tuple("foo", true),
                        Tuple.tuple("hurr", true));
        softly.assertThat(fileSystem.listDirectory("/foo"))
                .describedAs("mkdir /hurr/durr; ls /foo")
                .extracting("name", "directory")
                .containsExactlyInAnyOrder(
                        Tuple.tuple("bar", true),
                        Tuple.tuple("bee", true));
        softly.assertThat(fileSystem.listDirectory("/foo/bar"))
                .describedAs("mkdir /hurr/durr; ls /foo/bar")
                .extracting("name", "directory")
                .containsExactlyInAnyOrder(
                        Tuple.tuple("baz", true));
        softly.assertThat(fileSystem.listDirectory("/foo/bar/baz"))
                .describedAs("mkdir /hurr/durr; ls /foo/bar/baz")
                .isEmpty();
        softly.assertThat(fileSystem.listDirectory("/foo/bee"))
                .describedAs("mkdir /hurr/durr; ls /foo/bee")
                .isEmpty();
        softly.assertThat(fileSystem.listDirectory("/hurr"))
                .describedAs("mkdir /hurr/durr; ls /hurr")
                .extracting("name", "directory")
                .containsExactlyInAnyOrder(
                        Tuple.tuple("durr", true));
        softly.assertThat(fileSystem.listDirectory("/hurr/durr"))
                .describedAs("mkdir /hurr/durr; ls /hurr/durr")
                .isEmpty();
    }

    @Test
    public void multipleFileCreationAndDeletion() throws InterruptedException {
        List<String> files = Arrays.asList(
                "/a",
                "/ab",
                "/abc",
                "/abcd",
                "/abcde",
                "/abcdef",
                "/abcdefg",
                "/abcdefgh",
                "/abcdefghi",
                "/abcdefghij",
                "/abcdefghijk",
                "/abcdefghijkl",
                "/abcdefghijklm");
        files.forEach(this::checkCreateAndOpen);

        List<String> directories = Arrays.asList(
                "/z",
                "/zy",
                "/zyx",
                "/zyxw",
                "/zyxwv",
                "/zyxwvu",
                "/zyxwvut",
                "/zyxwvuts",
                "/zyxwvutsr",
                "/zyxwvutsrq",
                "/zyxwvutsrqo",
                "/zyxwvutsrqon",
                "/zyxwvutsrqonm");
        for (String directory : directories) {
            try {
                fileSystem.makeDirectory(directory);
                files.forEach(filePath -> checkCreateAndOpen(directory + filePath));
                fileSystem.deleteDirectory(directory);
            } catch (RuntimeException exc) {
                throw new IllegalStateException("In directory " + directory, exc);
            }
        }

        fileSystem.makeDirectory("/root");
        for (String oldDirPath : directories) {
            String dirPath = "/root" + oldDirPath;
            try {
                fileSystem.makeDirectory(dirPath);
                files.forEach(filePath -> checkCreateAndOpen(dirPath + filePath));
                fileSystem.deleteDirectory(dirPath);
            } catch (RuntimeException exc) {
                throw new IllegalStateException("In directory " + dirPath, exc);
            }
        }
    }

    private void checkCreateAndOpen(String path) {
        softly.assertThatCode(
                () -> {
                    fileSystem.makeFile(path);
                    fileSystem.openForWriting(path).close();
                    fileSystem.openForReading(path).close();
                    fileSystem.deleteFile(path);
                })
                .describedAs("Path: " + path)
                .doesNotThrowAnyException();
    }

    @Test
    public void writeOrAppend() throws IOException, InterruptedException {
        fileSystem.makeFile("/data.txt");

        try (BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(fileSystem.openForWriting("/data.txt"))))
        {
            writer.write("write");
        }
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(fileSystem.openForReading("/data.txt"))))
        {
            softly.assertThat(reader.readLine()).isEqualTo("write");
        }

        try (BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(fileSystem.openForAppending("/data.txt"))))
        {
            writer.write("123append");
        }
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(fileSystem.openForReading("/data.txt"))))
        {
            softly.assertThat(reader.readLine()).isEqualTo("write123append");
        }

        try (BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(fileSystem.openForWriting("/data.txt"))))
        {
            writer.write("rewrite");
        }
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(fileSystem.openForReading("/data.txt"))))
        {
            softly.assertThat(reader.readLine()).isEqualTo("rewrite3append");
        }
    }

    @Test
    public void readWriteFileSmallString() throws IOException, InterruptedException {
        fileSystem.makeFile("/data.txt");
        String writtenContent = "Hello!";
        if (writtenContent.length() >= BLOCK_SIZE - ChunkListStorage.METADATA_SIZE) {
            throw new AssumptionViolatedException("Too big string for one block");
        }
        try (BufferedWriter bufferedWriter = new BufferedWriter(
                new OutputStreamWriter(fileSystem.openForWriting("/data.txt"))))
        {
            bufferedWriter.write(writtenContent);
        }
        String readContent;
        try (InputStream inputStream = fileSystem.openForReading("/data.txt")) {
            readContent = IOUtils.toString(inputStream, Charsets.UTF_8.name());
        }
        softly.assertThat(readContent).isEqualTo(writtenContent);
    }

    @Test
    public void readWriteFileSeveralChunks() throws IOException, InterruptedException {
        fileSystem.makeFile("/data.txt");
        String writtenContent = String.join("\n", Collections.nCopies(5, "Hello world!\nHurr durr!\nHerp derp\n"));
        if (writtenContent.length() < (BLOCK_SIZE - ChunkListStorage.METADATA_SIZE) * 5) {
            throw new AssumptionViolatedException("Too small string for testing several blocks");
        }
        try (OutputStream outputStream = fileSystem.openForWriting("/data.txt")) {
            outputStream.write(writtenContent.getBytes());
        }
        byte[] readContentBytes = new byte[writtenContent.getBytes().length];
        try (InputStream inputStream = fileSystem.openForReading("/data.txt")) {
            int readCount = inputStream.read(readContentBytes);
            softly.assertThat(readCount).isEqualTo(readContentBytes.length);
            softly.assertThat(new String(readContentBytes)).isEqualTo(writtenContent);
            softly.assertThat(inputStream.read()).isNegative();
        }
    }

    @Test
    public void deleteFile() throws InterruptedException {
        fileSystem.makeFile("/data1.txt");
        fileSystem.makeFile("/data2.txt");
        fileSystem.makeFile("/data3.txt");

        fileSystem.deleteFile("/data2.txt");
        softly.assertThat(fileSystem.listDirectory("/"))
                .describedAs("Deleted /data2.txt (from the middle)")
                .extracting("name")
                .containsExactlyInAnyOrder("data1.txt", "data3.txt");

        fileSystem.deleteFile("/data3.txt");
        softly.assertThat(fileSystem.listDirectory("/"))
                .describedAs("Deleted /data3.txt (from the end)")
                .extracting("name")
                .containsExactlyInAnyOrder("data1.txt");

        fileSystem.deleteFile("/data1.txt");
        softly.assertThat(fileSystem.listDirectory("/"))
                .describedAs("Deleted /data1.txt (the only file)")
                .isEmpty();

        softly.assertThatCode(() -> fileSystem.deleteFile("/sfsdfds"))
                .isInstanceOf(NoSuchFileOrDirectory.class);
    }

    @Test
    public void deleteDirectory() throws InterruptedException {
        fileSystem.makeDirectory("/foo");
        fileSystem.makeDirectory("/foo/bar");
        fileSystem.makeDirectory("/baz");

        softly.assertThatCode(() -> fileSystem.deleteDirectory("/sfdfgfd"))
                .describedAs("rmdir /sfdfgfd")
                .isInstanceOf(NoSuchFileOrDirectory.class);
        softly.assertThatCode(() -> fileSystem.deleteDirectory("/sfdf/gfd"))
                .describedAs("rmdir /sfdf/gfd")
                .isInstanceOf(NoSuchFileOrDirectory.class);
        softly.assertThatCode(() -> fileSystem.deleteDirectory("/foo/bar/sfdfgfd"))
                .describedAs("rmdir /foo/bar/sfdfgfd")
                .isInstanceOf(NoSuchFileOrDirectory.class);
        softly.assertThatCode(() -> fileSystem.deleteDirectory("/foo/bar/sfdf/gfd"))
                .describedAs("rmdir /foo/bar/sfdf/gfd")
                .isInstanceOf(NoSuchFileOrDirectory.class);

        softly.assertThat(fileSystem.listDirectory("/"))
                .describedAs("ls /")
                .extracting("name")
                .containsExactlyInAnyOrder("foo", "baz");
        softly.assertThat(fileSystem.listDirectory("/foo"))
                .describedAs("ls /foo")
                .extracting("name")
                .containsExactlyInAnyOrder("bar");

        fileSystem.deleteDirectory("/baz");
        softly.assertThat(fileSystem.listDirectory("/"))
                .describedAs("rmdir /baz; ls /")
                .extracting("name")
                .containsExactlyInAnyOrder("foo");
        softly.assertThat(fileSystem.listDirectory("/foo"))
                .describedAs("rmdir /baz; ls /foo")
                .extracting("name")
                .containsExactlyInAnyOrder("bar");

        fileSystem.deleteDirectory("/foo/bar");
        softly.assertThat(fileSystem.listDirectory("/"))
                .describedAs("rmdir /baz; rmdir /foo/bar; ls /")
                .extracting("name")
                .containsExactlyInAnyOrder("foo");
        softly.assertThat(fileSystem.listDirectory("/foo"))
                .describedAs("rmdir /baz; rmdir /foo/bar; ls /foo")
                .extracting("name")
                .isEmpty();

        fileSystem.deleteDirectory("/foo");
        softly.assertThat(fileSystem.listDirectory("/"))
                .describedAs("rmdir /baz; rmdir /foo/bar; rmdir /foo; ls /")
                .extracting("name")
                .isEmpty();
    }

    @Test
    public void testManyNestedDirectories() throws InterruptedException {
        final int count = 20;
        final StringBuilder path = new StringBuilder("/");
        for (int i = 1; i <= count; ++i) {
            path.append(i);
            Assertions.assertThatCode(() -> fileSystem.makeDirectory(path.toString()))
                    .describedAs("Path: " + path)
                    .doesNotThrowAnyException();
            path.append("/");
        }

        StringBuilder path2 = new StringBuilder("/");
        for (int i = 1; i <= count; ++i) {
            softly.assertThat(fileSystem.listDirectory(path2.toString()))
                    .describedAs("ls " + path2.toString())
                    .extracting("name")
                    .containsExactly(Integer.toString(i));
            path2.append(i).append("/");
        }
        softly.assertThat(fileSystem.listDirectory(path2.toString()))
                .describedAs("ls " + path2.toString())
                .isEmpty();
    }

    @Test
    public void copyFile() throws InterruptedException, IOException {
        fileSystem.makeFile("/src");
        String contents = "Hello world!";
        try (OutputStreamWriter writer = new OutputStreamWriter(fileSystem.openForWriting("/src"))) {
            writer.write(contents);
        }
        fileSystem.makeFile("/dst");
        try (InputStream reader = fileSystem.openForReading("/src");
             OutputStream writer = fileSystem.openForWriting("/dst"))
        {
            IOUtils.copy(reader, writer);
        }
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(fileSystem.openForReading("/src")))) {
            String line = reader.readLine();
            softly.assertThat(line).isEqualTo(contents);
        }
    }

    @Test
    public void inputStreamTraverse() throws InterruptedException, IOException {
        fileSystem.makeFile("/file");
        try (OutputStreamWriter writer = new OutputStreamWriter(fileSystem.openForWriting("/file"))) {
            writer.write("one\ntwo\nthree");
        }

        try (InputStream stream = fileSystem.openForReading("/file")) {
            softly.assertThat(stream.markSupported()).isTrue();

            softly.assertThat(readLine(stream)).isEqualTo("one");

            stream.mark(100);
            long skipped = stream.skip("two\n".length());
            softly.assertThat(skipped).isEqualTo("two\n".length());

            softly.assertThat(readLine(stream)).isEqualTo("three");

            stream.reset();
            softly.assertThat(readLine(stream)).isEqualTo("two");
            softly.assertThat(readLine(stream)).isEqualTo("three");

            stream.reset();
            softly.assertThat(stream.skip(100)).isEqualTo("two\nthree".length());
            softly.assertThat(stream.skip(100)).isEqualTo(0);
        }
    }

    @Test
    public void writeByOneByte() throws InterruptedException, IOException {
        String contents = String.join("", Collections.nCopies(10, "abcdef"));

        fileSystem.makeFile("/file");
        try (OutputStream stream = fileSystem.openForWriting("/file")) {
            for (byte c : contents.getBytes(StandardCharsets.US_ASCII)) {
                stream.write((int) c);
            }
        }

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(fileSystem.openForReading("/file")))) {
            String line = reader.readLine();
            softly.assertThat(line).isEqualTo(contents);
        }
    }

    @Test
    public void writeAndReadAnyByte() throws InterruptedException, IOException {
        byte[] writtenContents = new byte[256];
        for (int i = 0; i < writtenContents.length; ++i) {
            writtenContents[i] = (byte) (i & 0xff);
        }

        fileSystem.makeFile("/file");
        try (OutputStream stream = new BufferedOutputStream(fileSystem.openForWriting("/file"))) {
            stream.write(writtenContents);
        }

        byte[] readContents = new byte[writtenContents.length];

        try (InputStream reader = new BufferedInputStream(fileSystem.openForReading("/file"))) {
            int readCount = reader.read(readContents);
            softly.assertThat(readCount).isEqualTo(readContents.length);
        }

        softly.assertThat(readContents).isEqualTo(writtenContents);
    }
}