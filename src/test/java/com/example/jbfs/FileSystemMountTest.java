package com.example.jbfs;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.ParametersAreNonnullByDefault;

import com.example.jbfs.exceptions.NoSpaceLeft;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.JUnitSoftAssertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

@ParametersAreNonnullByDefault
public class FileSystemMountTest {
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Rule
    public JUnitSoftAssertions softly = new JUnitSoftAssertions();

    @Test
    public void testMountAndUnmount() throws IOException {
        Path backendFile = temporaryFolder.newFile().toPath();
        FileBackedFileSystem.format(backendFile, 4096, 256);
        FileBackedFileSystem.mount(backendFile).close();

        for (int i = 1; i <= 10; ++i) {
            softly.assertThatCode(() -> FileBackedFileSystem.mount(backendFile).close())
                    .describedAs("Remount: attempt " + i)
                    .doesNotThrowAnyException();
        }
    }

    @Test
    public void testResize() throws IOException {
        AtomicInteger counter = new AtomicInteger(0);

        Path backendFile = temporaryFolder.newFile().toPath();
        int blockSize = 64;
        int initialSize = 4096;
        FileBackedFileSystem.format(backendFile, initialSize, blockSize);
        FileSystem fileSystem = FileBackedFileSystem.mount(backendFile);

        Assertions.assertThatCode(
                () -> {
                    while (counter.incrementAndGet() < 100_000) {
                        String path = "file" + counter.get();
                        fileSystem.makeFile(path);
                        try (OutputStream stream = fileSystem.openForWriting(path)) {
                            stream.write('x');
                        }
                    }
                })
                .isInstanceOf(NoSpaceLeft.class);

        int countBeforeResize = counter.get();
        fileSystem.resize(initialSize * 16);

        while (counter.incrementAndGet() < countBeforeResize * 2) {
            String path = "file" + counter.get();
            Assertions.assertThatCode(
                    () -> {
                        fileSystem.makeFile(path);
                        try (OutputStream stream = fileSystem.openForWriting(path)) {
                            stream.write('x');
                        }
                    })
                    .describedAs(String.format("countBeforeResize = %d, countBeforeResize * 2 = %d, path = %s",
                            countBeforeResize, countBeforeResize * 2, path))
                    .doesNotThrowAnyException();
        }
    }
}
