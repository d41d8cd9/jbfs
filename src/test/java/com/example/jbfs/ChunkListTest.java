package com.example.jbfs;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.annotation.ParametersAreNonnullByDefault;

import com.google.common.base.Preconditions;
import org.assertj.core.api.JUnitSoftAssertions;
import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

@ParametersAreNonnullByDefault
public class ChunkListTest {
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Rule
    public JUnitSoftAssertions softly = new JUnitSoftAssertions();

    private ChunkList chunkList;
    private TestAllocator allocator;

    @Before
    public void setUp() {
        long blockCount = 8;
        int blockSize = 44;

        File file;
        FileChannel fileChannel;
        try {
            file = this.temporaryFolder.newFile();
            fileChannel = FileChannel.open(file.toPath(), StandardOpenOption.READ, StandardOpenOption.WRITE);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        // This is single-thread test, so passing file channel without adapter is safe.
        FileChannelAdaptor fileChannelAdaptor = new FileChannelAdaptor(fileChannel);
        this.allocator = new FileAllocator(fileChannel, blockCount, blockSize);
        this.chunkList = new ChunkList(fileChannelAdaptor, this.allocator, this.allocator.allocate());
    }

    private List<Integer> readAll() {
        try (ChunkList.Mark mark = chunkList.mark()) {
            List<Integer> result = new ArrayList<>();
            while (!chunkList.flipToChunkStartAndCheckIsListEnd()) {
                result.add((int) chunkList.readByte());
            }
            return result;
        }
    }

    @Test
    public void atChunkListEndForEmpty() {
        softly.assertThat(chunkList.flipToChunkStartAndCheckIsListEnd()).isTrue();
    }

    @Test
    public void atChunkListStartForEmpty() {
        softly.assertThat(chunkList.flipToChunkEndAndCheckIsListStart()).isTrue();
    }

    @Test
    public void replaceForEmpty() {
        softly.assertThatCode(() -> chunkList.replaceByte((byte) 1)).isInstanceOf(IndexOutOfBoundsException.class);
    }

    @Test
    public void expandAndWrite1Byte() {
        try (ChunkList.Mark rootMark = chunkList.mark()) {
            chunkList.expand(1);
            chunkList.replaceByte((byte) 12);
            softly.assertThat(allocator.blocks(1).get(0)).startsWith(
                    0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                    0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                    0, 0, 0, 1,  // size
                    12,  // data
                    0, 0, 0, 0, 0, 0, 0  // garbage
            );

            try (ChunkList.Mark mark = chunkList.mark()) {
                chunkList.expand(1);
                chunkList.replaceByte((byte) 34);
                softly.assertThat(allocator.blocks(1).get(0)).startsWith(
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 2,  // size
                        12, 34, // data
                        0, 0, 0, 0, 0, 0  // garbage
                );
            }

            chunkList.expand(1);
            chunkList.replaceByte((byte) 56);
            softly.assertThat(allocator.blocks(1).get(0)).startsWith(
                    0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                    0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                    0, 0, 0, 3,  // size
                    12, 56, 34, // data
                    0, 0, 0, 0, 0  // garbage
            );
        }
        try (ChunkList.Mark rootMark = chunkList.mark()) {
            chunkList.expand(1);
            chunkList.replaceByte((byte) 78);
            softly.assertThat(allocator.blocks(1).get(0)).startsWith(
                    0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                    0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                    0, 0, 0, 4,  // size
                    78, 12, 56, 34, // data
                    0, 0, 0, 0  // garbage
            );
        }
        chunkList.forwardStrict(4);
        chunkList.expand(1);
        chunkList.replaceByte((byte) 90);
        softly.assertThat(allocator.blocks(1).get(0)).startsWith(
                0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                0, 0, 0, 5,  // size
                78, 12, 56, 34, 90, // data
                0, 0, 0, 0  // garbage
        );
    }

    @Test
    public void shrink1Byte() {
        chunkList.expand(10);
        try (ChunkList.Mark ignored = chunkList.mark()) {
            chunkList.replaceBytes(new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        }
        softly.assertThat(allocator.blocks(1))
                .describedAs("Before any shrinks")
                .containsExactly(new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 10,  // size
                        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,  // data
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  // garbage
                });

        try (ChunkList.Mark ignored = chunkList.mark()) {
            chunkList.forwardStrict(9);
            chunkList.shrink(1);
        }
        softly.assertThat(allocator.blocks(1))
                .describedAs("Shrink at the end")
                .containsExactly(new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 9,  // size
                        1, 2, 3, 4, 5, 6, 7, 8, 9,  // data
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  // garbage
                });

        try (ChunkList.Mark ignored = chunkList.mark()) {
            chunkList.forwardStrict(4);
            chunkList.shrink(1);
            chunkList.replaceByte((byte) 66);
        }
        softly.assertThat(allocator.blocks(1))
                .describedAs("Shrink at the middle")
                .containsExactly(new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 8,  // size
                        1, 2, 3, 4, 66, 7, 8, 9,  // data
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  // garbage
                });

        chunkList.shrink(1);
        chunkList.replaceByte((byte) 22);
        softly.assertThat(allocator.blocks(1))
                .describedAs("Shrink at the start")
                .containsExactly(new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 7,  // size
                        22, 3, 4, 66, 7, 8, 9,  // data
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  // garbage
                });
        softly.assertThat(allocator.getDeallocated()).isEmpty();
    }

    @Test
    public void expandAndWrite5Bytes() {
        try (ChunkList.Mark rootMark = chunkList.mark()) {
            try (ChunkList.Mark mark = chunkList.mark()) {
                chunkList.expand(5);
                chunkList.replaceBytes(new byte[]{1, 2, 3, 4, 5});
                softly.assertThat(allocator.blocks(1).get(0)).startsWith(
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 5,  // size
                        1, 2, 3, 4, 5,  // data
                        0, 0, 0, 0  // garbage
                );

                chunkList.expand(5);
                chunkList.replaceBytes(new byte[]{6, 7, 8, 9, 10});
                softly.assertThat(allocator.blocks(1).get(0)).startsWith(
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 10,  // size
                        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,  // data
                        0, 0, 0  // garbage
                );
            }

            chunkList.forwardStrict(2);
            chunkList.expand(5);
            chunkList.replaceBytes(new byte[]{11, 12, 13, 14, 15});
            softly.assertThat(allocator.blocks(1).get(0)).startsWith(
                    0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                    0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                    0, 0, 0, 15,  // size
                    1, 2, 11, 12, 13, 14, 15, 3, 4, 5, 6, 7, 8, 9, 10,  // data
                    0, 0, 0, 0, 0  // garbage
            );
        }
        chunkList.expand(5);
        chunkList.replaceBytes(new byte[]{16, 17, 18, 19, 20});
        softly.assertThat(allocator.blocks(1).get(0)).startsWith(
                0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                0, 0, 0, 20,  // size
                16, 17, 18, 19, 20, 1, 2, 11, 12, 13, 14, 15, 3, 4, 5, 6, 7, 8, 9, 10,  // data
                0, 0  // garbage
        );
    }

    @Test
    public void shrink5Bytes() {
        chunkList.expand(10);
        try (ChunkList.Mark ignored = chunkList.mark()) {
            chunkList.replaceBytes(new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        }
        softly.assertThat(allocator.blocks(1))
                .describedAs("Before any shrinks")
                .containsExactly(new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 10,  // size
                        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,  // data
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  // garbage
                });

        try (ChunkList.Mark ignored = chunkList.mark()) {
            chunkList.forwardStrict(3);
            chunkList.shrink(5);
        }

        softly.assertThat(allocator.blocks(1))
                .describedAs("Before any shrinks")
                .containsExactly(new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 5,  // size
                        1, 2, 3, 9, 10,  // garbage
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  // garbage
                });
        chunkList.shrink(5);
        softly.assertThat(allocator.blocks(1))
                .describedAs("Before any shrinks")
                .containsExactly(new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 0,  // size
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  // garbage
                });
    }

    @Test
    public void expandOneChunk() {
        try (ChunkList.Mark mark = chunkList.mark()) {
            chunkList.expand(15);
            for (byte i = -1; i >= -15; --i) {
                chunkList.replaceByte(i);
            }

            chunkList.expand(15);
            for (byte i = 1; i <= 15; ++i) {
                chunkList.replaceByte(i);
            }
        }
        softly.assertThat(readAll()).containsExactly(
                -1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12, -13, -14, -15,
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
        softly.assertThat(allocator.blocks(2)).containsExactly(
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 24,  // size
                        -1, -2, -3, -4, -5, -6, -7, -8, -9, -10,  // data
                        -11, -12, -13, -14, -15, 1, 2, 3, 4, 5,  // data
                        6, 7, 8, 9 // data
                },
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 1,  // previous chunk offset
                        0, 0, 0, 6,  // size
                        10, 11, 12, 13, 14, 15,  // data
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  // garbage
                });

        try (ChunkList.Mark mark = chunkList.mark()) {
            chunkList.forwardStrict(10);
            chunkList.expand(10);
            for (byte i = 15; i < 25; ++i) {
                chunkList.replaceByte(i);
            }
        }

        softly.assertThat(readAll()).containsExactly(
                -1, -2, -3, -4, -5, -6, -7, -8, -9, -10,
                15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
                -11, -12, -13, -14, -15,
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
        softly.assertThat(allocator.blocks(3)).containsExactly(
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 2 + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 24,  // size
                        -1, -2, -3, -4, -5, -6, -7, -8, -9, -10,  // data
                        15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
                        -11, -12, -13, -14,
                },
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 2 + 1),  // previous chunk offset
                        0, 0, 0, 6,  // size
                        10, 11, 12, 13, 14, 15,  // data
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // garbage
                },
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 1,  // previous chunk offset
                        0, 0, 0, 10,  // size
                        -15, 1, 2, 3, 4, 5, 6, 7, 8, 9, // data
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // garbage
                }
        );
    }

    @Test
    public void expandThreeChunks() {
        byte count = 70;
        List<Integer> expected = new ArrayList<>(count);
        Preconditions.checkState(count > (allocator.getBlockSize() - ChunkListStorage.METADATA_SIZE) * 2);
        Preconditions.checkState(count < (allocator.getBlockSize() - ChunkListStorage.METADATA_SIZE) * 3);
        try (ChunkList.Mark ignored = chunkList.mark()) {
            chunkList.expand(count);
            for (byte i = 1; i <= count; ++i) {
                expected.add((int) i);
                chunkList.replaceByte(i);
            }
        }
        softly.assertThat(readAll()).isEqualTo(expected);
        softly.assertThat(allocator.blocks(4)).containsExactly(
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 24,  // size
                        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,  // data
                        11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                        21, 22, 23, 24,
                },
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 2 + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 1,  // previous chunk offset
                        0, 0, 0, 24,  // size
                        25, 26, 27, 28, 29, 30, 31, 32, 33, 34,  // data
                        35, 36, 37, 38, 39, 40, 41, 42, 43, 44,
                        45, 46, 47, 48,
                },
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() + 1),  // previous chunk offset
                        0, 0, 0, 22,  // size
                        49, 50, 51, 52, 53, 54, 55, 56, 57, 58,  // data
                        59, 60, 61, 62, 63, 64, 65, 66, 67, 68,
                        69, 70,
                        0, 0,  // garbage
                },
                new byte[allocator.getBlockSize()]);
    }

    @Test
    public void shrinkOneChunkFromMiddle() {
        try (ChunkList.Mark ignored = chunkList.mark()) {
            chunkList.expand(50);
            for (byte i = 1; i <= 50; ++i) {
                chunkList.replaceByte(i);
            }
        }
        softly.assertThat(allocator.blocks(3)).containsExactly(
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 24,  // size
                        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,  // data
                        11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                        21, 22, 23, 24,
                },
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 2 + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 1,  // previous chunk offset
                        0, 0, 0, 24,  // size
                        25, 26, 27, 28, 29, 30, 31, 32, 33, 34,  // data
                        35, 36, 37, 38, 39, 40, 41, 42, 43, 44,
                        45, 46, 47, 48,
                },
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() + 1),  // previous chunk offset
                        0, 0, 0, 2,  // size
                        49, 50,  // data
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
                });

        try (ChunkList.Mark mark = chunkList.mark()) {
            chunkList.forwardStrict(15);
            chunkList.shrink(20);
        }

        softly.assertThat(readAll()).containsExactly(
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
                36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50);
        softly.assertThat(allocator.blocks(3)).containsExactly(
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 15,  // size
                        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,  // data
                        11, 12, 13, 14, 15,
                        0, 0, 0, 0, 0, 0, 0, 0, 0,  // garbage
                },
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 2 + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 1,  // previous chunk offset
                        0, 0, 0, 13,  // size
                        36, 37, 38, 39, 40, 41, 42, 43, 44, // data
                        45, 46, 47, 48,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // garbage
                },
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() + 1),  // previous chunk offset
                        0, 0, 0, 2,  // size
                        49, 50,  // data
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
                });
        softly.assertThat(allocator.getDeallocated()).isEmpty();
    }

    private void write100bytes() {
        try (ChunkList.Mark ignored = chunkList.mark()) {
            chunkList.expand(100);
            for (byte i = 1; i <= 100; ++i) {
                chunkList.replaceByte(i);
            }
        }
        softly.assertThat(allocator.blocks(5)).containsExactly(
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 24,  // size
                        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,  // data
                        11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                        21, 22, 23, 24,
                },
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 2 + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 1,  // previous chunk offset
                        0, 0, 0, 24,  // size
                        25, 26, 27, 28, 29, 30, 31, 32, 33, 34,  // data
                        35, 36, 37, 38, 39, 40, 41, 42, 43, 44,
                        45, 46, 47, 48,
                },
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 3 + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() + 1),  // previous chunk offset
                        0, 0, 0, 24,  // size
                        49, 50, 51, 52, 53, 54, 55, 56, 57, 58,  // data
                        59, 60, 61, 62, 63, 64, 65, 66, 67, 68,
                        69, 70, 71, 72
                },
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 4 + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 2 + 1),  // previous chunk offset
                        0, 0, 0, 24,
                        73, 74, 75, 76, 77, 78, 79, 80, 81, 82, // data
                        83, 84, 85, 86, 87, 88, 89, 90, 91, 92,
                        93, 94, 95, 96,
                },
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 3 + 1),  // previous chunk offset
                        0, 0, 0, 4,
                        97, 98, 99, 100,  // data
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // garbage
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                });
    }

    @Test
    public void shrinkThreeChunksFromMiddle() {
        write100bytes();

        try (ChunkList.Mark mark = chunkList.mark()) {
            chunkList.forwardStrict(15);
            chunkList.shrink(60);
        }

        softly.assertThat(readAll()).isEqualTo(
                Stream.concat(IntStream.range(1, 16).boxed(), IntStream.range(76, 101).boxed())
                        .collect(Collectors.toList()));
        softly.assertThat(allocator.blocks(5)).containsExactly(
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 3 + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 15,  // size
                        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,  // data
                        11, 12, 13, 14, 15,
                        0, 0, 0, 0, 0, 0, 0, 0, 0,  // garbage
                },
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()],
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 4 + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 1,  // previous chunk offset
                        0, 0, 0, 21,
                        76, 77, 78, 79, 80, 81, 82, // data
                        83, 84, 85, 86, 87, 88, 89, 90, 91, 92,
                        93, 94, 95, 96,
                        0, 0, 0, // garbage
                },
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 3 + 1),  // previous chunk offset
                        0, 0, 0, 4,
                        97, 98, 99, 100,  // data
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // garbage
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                });
        softly.assertThat(allocator.getDeallocated()).containsExactlyInAnyOrder(
                (long) allocator.getBlockSize() + 1,
                (long) allocator.getBlockSize() * 2 + 1);
    }

    @Test
    public void shrinkFromMiddleUntilTheEnd() {
        write100bytes();

        try (ChunkList.Mark mark = chunkList.mark()) {
            chunkList.forwardStrict(15);
            chunkList.shrink(85);
        }

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(readAll()).isEqualTo(
                IntStream.range(1, 16).boxed().collect(Collectors.toList()));
        softAssertions.assertThat(allocator.blocks(5)).containsExactly(
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 15,  // size
                        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,  // data
                        11, 12, 13, 14, 15,
                        0, 0, 0, 0, 0, 0, 0, 0, 0,  // garbage
                },
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()]);
        softAssertions.assertThat(allocator.getDeallocated()).containsExactlyInAnyOrder(
                (long) allocator.getBlockSize() + 1,
                (long) allocator.getBlockSize() * 2 + 1,
                (long) allocator.getBlockSize() * 3 + 1,
                (long) allocator.getBlockSize() * 4 + 1);
        softAssertions.assertAll();

        // Something may be written after shrink
        try (ChunkList.Mark mark = chunkList.mark()) {
            chunkList.forwardToEnd();
            chunkList.expand(3);
            chunkList.replaceByte((byte) 55);
            chunkList.replaceByte((byte) 66);
            chunkList.replaceByte((byte) 77);
        }
        softly.assertThat(readAll()).isEqualTo(
                Stream.concat(IntStream.range(1, 16).boxed(), Stream.of(55, 66, 77))
                        .collect(Collectors.toList()));
        softly.assertThat(allocator.blocks(5)).containsExactly(
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 18,  // size
                        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,  // data
                        11, 12, 13, 14, 15, 55, 66, 77,
                        0, 0, 0, 0, 0, 0,  // garbage
                },
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()]);
    }

    @Test
    public void shrinkChunksFromListStart() {
        write100bytes();

        chunkList.shrink(75);

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(readAll()).isEqualTo(
                IntStream.range(76, 101).boxed().collect(Collectors.toList()));
        softAssertions.assertThat(allocator.blocks(5)).containsExactly(
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 4 + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 21,
                        76, 77, 78, 79, 80, 81, 82, // data
                        83, 84, 85, 86, 87, 88, 89, 90, 91, 92,
                        93, 94, 95, 96,
                        0, 0, 0,  // garbage
                },
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()],
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 1,  // previous chunk offset
                        0, 0, 0, 4,
                        97, 98, 99, 100,  // data
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // garbage
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                });
        softAssertions.assertThat(allocator.getDeallocated())
                .containsExactly(
                        (long) allocator.getBlockSize() + 1,
                        (long) allocator.getBlockSize() * 2 + 1,
                        (long) allocator.getBlockSize() * 3 + 1);
        softAssertions.assertAll();

        allocator.getDeallocated().clear();
        chunkList.shrink(23);

        softly.assertThat(readAll()).containsExactly(99, 100);
        softly.assertThat(allocator.blocks(5)).containsExactly(
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 2,
                        99, 100,  // data
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // garbage
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0,
                },
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()]);
        softly.assertThat(allocator.getDeallocated()).containsExactlyInAnyOrder(
                (long) allocator.getBlockSize() * 4 + 1);
    }

    @Test
    public void shrinkWholeChunk() {
        write100bytes();

        try (ChunkList.Mark mark = chunkList.mark()) {
            Preconditions.checkState(allocator.getBlockSize() - ChunkListStorage.METADATA_SIZE == 24);
            chunkList.forwardStrict(24);
            chunkList.shrink(24);
        }

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(readAll()).isEqualTo(
                Stream.concat(IntStream.range(1, 25).boxed(), IntStream.range(49, 101).boxed())
                        .collect(Collectors.toList()));
        softAssertions.assertThat(allocator.blocks(5)).containsExactly(
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 2 + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 24,  // size
                        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,  // data
                        11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                        21, 22, 23, 24,
                },
                new byte[allocator.getBlockSize()],
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 3 + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 1,  // previous chunk offset
                        0, 0, 0, 24,  // size
                        49, 50, 51, 52, 53, 54, 55, 56, 57, 58,  // data
                        59, 60, 61, 62, 63, 64, 65, 66, 67, 68,
                        69, 70, 71, 72
                },
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 4 + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 2 + 1),  // previous chunk offset
                        0, 0, 0, 24,
                        73, 74, 75, 76, 77, 78, 79, 80, 81, 82, // data
                        83, 84, 85, 86, 87, 88, 89, 90, 91, 92,
                        93, 94, 95, 96,
                },
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 3 + 1),  // previous chunk offset
                        0, 0, 0, 4,
                        97, 98, 99, 100,  // data
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // garbage
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                });
        softAssertions.assertThat(allocator.getDeallocated()).containsExactlyInAnyOrder(
                (long) allocator.getBlockSize() + 1);
        softAssertions.assertAll();

        allocator.clearDeallocated();
        try (ChunkList.Mark mark = chunkList.mark()) {
            chunkList.forwardStrict(24);
            chunkList.shrink(48);
        }

        softly.assertThat(readAll()).isEqualTo(
                Stream.concat(IntStream.range(1, 25).boxed(), IntStream.range(97, 101).boxed())
                        .collect(Collectors.toList()));
        softly.assertThat(allocator.blocks(5)).containsExactly(
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, (byte) (allocator.getBlockSize() * 4 + 1),  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 0,  // previous chunk offset
                        0, 0, 0, 24,  // size
                        1, 2, 3, 4, 5, 6, 7, 8, 9, 10,  // data
                        11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                        21, 22, 23, 24,
                },
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()],
                new byte[]{
                        0, 0, 0, 0, 0, 0, 0, 0,  // next chunk offset
                        0, 0, 0, 0, 0, 0, 0, 1,  // previous chunk offset
                        0, 0, 0, 4,
                        97, 98, 99, 100,  // data
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // garbage
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                });
        softly.assertThat(allocator.getDeallocated()).containsExactlyInAnyOrder(
                (long) allocator.getBlockSize() * 2 + 1,
                (long) allocator.getBlockSize() * 3 + 1);
    }

    @Test
    public void shrinkWholeChunkList() {
        write100bytes();

        try (ChunkList.Mark mark = chunkList.mark()) {
            chunkList.shrink(100);
        }

        softly.assertThat(readAll()).isEmpty();
        softly.assertThat(allocator.blocks(5)).containsExactly(
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()],
                new byte[allocator.getBlockSize()]);
        softly.assertThat(allocator.getDeallocated()).containsExactlyInAnyOrder(
                // first chunk can never be removed
                (long) allocator.getBlockSize() + 1,
                (long) allocator.getBlockSize() * 2 + 1,
                (long) allocator.getBlockSize() * 3 + 1,
                (long) allocator.getBlockSize() * 4 + 1);
    }

    @Test
    public void testForward() {
        int count = 10;
        try (ChunkList.Mark ignored = chunkList.mark()) {
            chunkList.expand(count);
            for (byte i = 1; i <= count; ++i) {
                chunkList.replaceByte(i);
            }
        }
        softly.assertThat(chunkList.readByte()).isEqualTo((byte) 1);
        chunkList.forwardStrict(1);
        softly.assertThat(chunkList.readByte()).isEqualTo((byte) 3);
        chunkList.forwardStrict(2);
        softly.assertThat(chunkList.readByte()).isEqualTo((byte) 6);
        chunkList.forwardStrict(3);
        softly.assertThat(chunkList.readByte()).isEqualTo((byte) 10);
    }

    @Test
    public void testRewind() {
        int count = 10;
        chunkList.expand(count);
        for (byte i = 1; i <= count; ++i) {
            chunkList.replaceByte(i);
        }
        softly.assertThat(chunkList.flipToChunkStartAndCheckIsListEnd()).isTrue();
        chunkList.rewindStrict(1);
        softly.assertThat(chunkList.readByte()).isEqualTo((byte) 10);
        chunkList.rewindStrict(5);
        softly.assertThat(chunkList.readByte()).isEqualTo((byte) 6);
        softly.assertThat(chunkList.readByte()).isEqualTo((byte) 7);
        chunkList.rewindStrict(4);
        softly.assertThat(chunkList.readByte()).isEqualTo((byte) 4);
        softly.assertThat(chunkList.readByte()).isEqualTo((byte) 5);
        chunkList.rewindStrict(5);
        softly.assertThat(chunkList.flipToChunkEndAndCheckIsListStart()).isTrue();
        softly.assertThat(chunkList.readByte()).isEqualTo((byte) 1);
        softly.assertThat(chunkList.flipToChunkEndAndCheckIsListStart()).isFalse();
        chunkList.rewindStrict(1);
        softly.assertThat(chunkList.readByte()).isEqualTo((byte) 1);
    }

    @Test
    public void testForwardToEnd() {
        int count = 5;
        try (ChunkList.Mark ignored = chunkList.mark()) {
            chunkList.expand(count);
            for (byte i = 1; i <= count; ++i) {
                chunkList.replaceByte(i);
            }
        }

        try (ChunkList.Mark ignored = chunkList.mark()) {
            chunkList.forwardToEnd();
            chunkList.expand(1);
            chunkList.replaceByte((byte) 106);
        }

        try (ChunkList.Mark ignored = chunkList.mark()) {
            softly.assertThat(readAll()).containsExactly(1, 2, 3, 4, 5, 106);
        }

        try (ChunkList.Mark ignored = chunkList.mark()) {
            chunkList.forwardStrict(3);
            chunkList.forwardToEnd();
            // several repeats does not break anything
            chunkList.forwardToEnd();
            chunkList.forwardToEnd();
            chunkList.expand(1);
            chunkList.replaceByte((byte) 107);
        }

        try (ChunkList.Mark ignored = chunkList.mark()) {
            softly.assertThat(readAll()).containsExactly(1, 2, 3, 4, 5, 106, 107);
        }
    }

    @Test
    public void deallocate1Block() {
        chunkList.deallocate();
        softly.assertThat(allocator.getDeallocated()).containsExactly(1L);
    }

    @Test
    public void deallocate5Blocks() {
        chunkList.expand((allocator.getBlockSize() - ChunkListStorage.METADATA_SIZE) * 5);
        chunkList.deallocate();
        softly.assertThat(allocator.getDeallocated()).containsExactlyInAnyOrder(
                1L,
                (long) allocator.getBlockSize() + 1,
                (long) allocator.getBlockSize() * 2 + 1,
                (long) allocator.getBlockSize() * 3 + 1,
                (long) allocator.getBlockSize() * 4 + 1);
    }

    abstract static class TestAllocator implements Allocator {
        final int blockSize;
        List<Long> deallocated = new ArrayList<>();

        TestAllocator(int blockSize) {
            this.blockSize = blockSize;
        }

        @Override
        public void deallocate(long offset) {
            deallocated.add(offset);
        }

        @Override
        public int getBlockSize() {
            return blockSize;
        }

        List<Long> getDeallocated() {
            return deallocated;
        }

        void clearDeallocated() {
            deallocated.clear();
        }

        abstract List<byte[]> blocks(long count);
    }

    static class FileAllocator extends TestAllocator {
        private final long limit;
        private final FileChannel fileChannel;
        private int counter = 1;

        FileAllocator(FileChannel fileChannel, long blockCount, int blockSize) {
            super(blockSize);
            this.fileChannel = fileChannel;
            this.limit = blockCount * blockSize + 1;
            try {
                fileChannel.truncate(limit);
                fileChannel.position(limit - 1);
                fileChannel.write(ByteBuffer.wrap(new byte[]{0}));
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }

        @Override
        public long allocate() {
            int result = counter;
            if (result > limit) {
                throw new IllegalStateException("This simple allocator is exceeded");
            }
            counter += blockSize;
            return result;
        }

        @Override
        public List<byte[]> blocks(long count) {
            List<byte[]> result = new ArrayList<>();

            try {
                long initialPosition = fileChannel.position();
                fileChannel.position(1);
                while (--count >= 0) {
                    byte[] block = new byte[blockSize];
                    fileChannel.read(ByteBuffer.wrap(block));
                    result.add(block);
                }
                fileChannel.position(initialPosition);
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
            return result;
        }
    }
}
