package com.example.jbfs;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.StandardOpenOption;

import javax.annotation.ParametersAreNonnullByDefault;

import org.assertj.core.api.JUnitSoftAssertions;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

@ParametersAreNonnullByDefault
public class FsBitSetTest {
    @ClassRule
    public static TemporaryFolder temporaryFolder = new TemporaryFolder();
    @Rule
    public JUnitSoftAssertions softly = new JUnitSoftAssertions();

    private FsBitSet makeBitSet(int bits) {
        try {
            File file = temporaryFolder.newFile();
            FileChannel channel = FileChannel.open(file.toPath(),
                    StandardOpenOption.READ, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
            int bytesInBlock = 2;
            Allocator allocator = new Allocator() {
                int counter = getBlockSize() + 1;

                @Override
                public long allocate() {
                    int result = counter;
                    counter += getBlockSize();
                    return result;
                }

                @Override
                public void deallocate(long offset) {
                    throw new UnsupportedOperationException();
                }

                @Override
                public int getBlockSize() {
                    return ChunkListStorage.METADATA_SIZE + bytesInBlock;
                }
            };
            int bytes = (bits + 7) / 8;
            int blockCount = (bytes + bytesInBlock - 1) / bytesInBlock + 1;
            int fileEnd = allocator.getBlockSize() * blockCount;
            channel.truncate(fileEnd);
            channel.position(fileEnd - 1);
            channel.write(ByteBuffer.wrap(new byte[]{0}));
            channel.position(1);
            ChunkList chunkList = new ChunkList(new FileChannelAdaptor(channel), allocator, 1);
            chunkList.expand(bytes);
            return new ChunkListBitSet(chunkList);
        } catch (IOException exc) {
            throw new UncheckedIOException(exc);
        }
    }

    @Test
    public void getSetOneBit8() {
        for (int i = 0; i < 8; ++i) {
            try {
                FsBitSet bitSet = makeBitSet(8);
                softly.assertThat(bitSet.get(i))
                        .describedAs(String.format("Bit %d at start", i))
                        .isFalse();
                bitSet.set(i, true);
                softly.assertThat(bitSet.get(i))
                        .describedAs(String.format("Bit %d after set to true", i))
                        .isTrue();
                bitSet.set(i, false);
                softly.assertThat(bitSet.get(i))
                        .describedAs(String.format("Bit %d after set to false", i))
                        .isFalse();
            } catch (RuntimeException exc) {
                throw new IllegalStateException("For bit " + i, exc);
            }
        }
    }

    @Test
    public void getSetOneBit128() {
        for (int i = 0; i < 128; ++i) {
            try {
                FsBitSet bitSet = makeBitSet(128);
                softly.assertThat(bitSet.get(i))
                        .describedAs(String.format("Bit %d at start", i))
                        .isFalse();
                bitSet.set(i, true);
                softly.assertThat(bitSet.get(i))
                        .describedAs(String.format("Bit %d after set to true", i))
                        .isTrue();
                bitSet.set(i, false);
                softly.assertThat(bitSet.get(i))
                        .describedAs(String.format("Bit %d after set to false", i))
                        .isFalse();
            } catch (RuntimeException exc) {
                throw new IllegalStateException("For bit " + i, exc);
            }
        }
    }

    @Test
    public void evenBitsSet32() {
        int i = 0;
        FsBitSet bitSet = makeBitSet(32);
        try {
            for (i = 0; i < 32; i += 2) {
                bitSet.set(i, true);
            }
            for (i = 0; i < 32; ++i) {
                softly.assertThat(bitSet.get(i))
                        .describedAs(String.format("For bit %d after set even to true", i))
                        .isEqualTo((i & 1) == 0);
            }
            for (i = 0; i < 32; i += 2) {
                bitSet.set(i, false);
            }
            for (i = 0; i < 32; ++i) {
                softly.assertThat(bitSet.get(i))
                        .describedAs(String.format("For bit %d after set even to false", i))
                        .isFalse();
            }
        } catch (RuntimeException exc) {
            throw new IllegalStateException("For bit " + i, exc);
        }
    }

    @Test
    public void nextSetBit() {
        for (int i = 0; i < 32; ++i) {
            try {
                FsBitSet bitSet = makeBitSet(32);
                bitSet.set(i, true);
                softly.assertThat(bitSet.nextSetBit(0))
                        .describedAs("For bit " + i)
                        .isEqualTo(i);
            } catch (RuntimeException exc) {
                throw new IllegalStateException("For bit " + i, exc);
            }
        }

        softly.assertThat(makeBitSet(32).nextSetBit(0))
                .describedAs("For all unset")
                .isEqualTo(-1);
    }

    @Test
    public void nextSetBitFromTheMiddle() {
        FsBitSet bitSet = makeBitSet(16);
        bitSet.set(5, true);
        bitSet.set(10, true);
        bitSet.set(12, true);
        int[] results = new int[]{
                5, 5, 5, 5, 5,
                5, 10, 10, 10, 10,
                10, 12, 12, -1, -1,
                -1,
        };
        for (int i = 0; i < 16; ++i) {
            try {
                softly.assertThat(bitSet.nextSetBit(i))
                        .describedAs("For bit " + i)
                        .isEqualTo(results[i]);
            } catch (RuntimeException exc) {
                throw new IllegalStateException("For bit " + i, exc);
            }
        }
    }

    @Test
    public void nextClearBit() {
        for (int testingBit = 0; testingBit < 32; ++testingBit) {
            try {
                FsBitSet bitSet = makeBitSet(32);
                for (int confusingBit = 0; confusingBit < 32; ++confusingBit) {
                    if (testingBit != confusingBit) {
                        bitSet.set(confusingBit, true);
                    }
                }
                softly.assertThat(bitSet.nextClearBit(0))
                        .describedAs("For bit " + testingBit)
                        .isEqualTo(testingBit);
            } catch (RuntimeException exc) {
                throw new IllegalStateException("For bit " + testingBit, exc);
            }
        }

        FsBitSet bitSet = makeBitSet(32);
        for (int i = 0; i < 32; ++i) {
            bitSet.set(i, true);
        }
        softly.assertThat(bitSet.nextClearBit(0))
                .describedAs("For all set")
                .isEqualTo(-1);
    }

    @Test
    public void nextClearBitFromTheMiddle() {
        FsBitSet bitSet = makeBitSet(16);
        bitSet.set(5, true);
        bitSet.set(6, true);
        bitSet.set(10, true);
        bitSet.set(12, true);
        bitSet.set(14, true);
        bitSet.set(15, true);
        int[] results = new int[]{
                0, 1, 2, 3, 4,
                7, 7, 7, 8, 9,
                11, 11, 13, 13, -1,
                -1,
        };
        for (int i = 0; i < 16; ++i) {
            try {
                softly.assertThat(bitSet.nextClearBit(i))
                        .describedAs("For bit " + i)
                        .isEqualTo(results[i]);
            } catch (RuntimeException exc) {
                throw new IllegalStateException("For bit " + i, exc);
            }
        }
    }

    @Test
    public void filledBitSet() {
        int bitSetSize = 16;
        FsBitSet bitSet = makeBitSet(bitSetSize);
        for (int i = 0; i < bitSetSize; i += 2) {
            softly.assertThat(bitSet.nextClearBit(0))
                    .describedAs("First loop. Bit " + i)
                    .isEqualTo(i == 0 ? 0 : 1);
            bitSet.set(i, true);
        }
        for (int i = 1; i < bitSetSize; i += 2) {
            softly.assertThat(bitSet.nextClearBit(0))
                    .describedAs("Second loop. Bit " + i)
                    .isEqualTo(i);
            bitSet.set(i, true);
        }
        softly.assertThat(bitSet.nextClearBit(0))
                .isEqualTo(-1);
    }
}