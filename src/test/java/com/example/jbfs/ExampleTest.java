package com.example.jbfs;

import javax.annotation.ParametersAreNonnullByDefault;

import com.example.jbfs.example.Example;
import org.assertj.core.api.Assertions;
import org.junit.Test;

@ParametersAreNonnullByDefault
public class ExampleTest {
    @Test
    public void exampleSmokeTest() {
        Assertions.assertThatCode(() -> Example.main(new String[0])).doesNotThrowAnyException();
    }
}
