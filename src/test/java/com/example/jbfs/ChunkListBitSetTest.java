package com.example.jbfs;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.annotation.ParametersAreNonnullByDefault;

import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

@ParametersAreNonnullByDefault
public class ChunkListBitSetTest {
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void sequential() throws IOException {
        runTest(totalBits -> IntStream.range(0, totalBits).toArray());
    }

    @Test
    public void random() throws IOException {
        Random random = new Random(31337);  // prime number
        runTest(totalBits -> {
            List<Integer> result = IntStream.range(0, totalBits).boxed().collect(Collectors.toList());
            Collections.shuffle(result, random);
            return result.stream().mapToInt(x -> x).toArray();
        });
    }

    private void runTest(IntFunction<int[]> positions) throws IOException {
        int blockSize = 32;
        int blocks = ChunkListBitSet.BUFFER_SIZE * 4;
        int fsSize = (blocks + 1) * blockSize;
        int totalBytes = blocks * (blockSize - ChunkListStorage.METADATA_SIZE);
        BitSet referenceBitSet = new BitSet(totalBytes * 8);

        Path backendFile = temporaryFolder.newFile().toPath();
        Allocator allocator = new TestAllocator(blockSize);
        try (FileChannel channel = FileChannel.open(backendFile,
                StandardOpenOption.READ, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING))
        {
            channel.truncate(fsSize);
            channel.position(fsSize - 1);
            channel.write(ByteBuffer.wrap(new byte[]{0}));
            FileChannelAdaptor adaptor = new FileChannelAdaptor(channel);
            ChunkListBitSet bitSet = new ChunkListBitSet(new ChunkList(adaptor, allocator, allocator.allocate()));
            bitSet.expand(totalBytes);

            // java.lang.BitSet allows to overpass specified limit, but ChunkListBitSet follows limit strictly
            for (int i : positions.apply(totalBytes * 8 - 1)) {
                String description = "At bit " + i;
                try {
                    Assertions.assertThat(bitSet.nextClearBit(0)).describedAs(description).isEqualTo(referenceBitSet.nextClearBit(0));
                    Assertions.assertThat(bitSet.nextClearBit(i)).describedAs(description).isEqualTo(referenceBitSet.nextClearBit(i));
                    bitSet.set(i, true);
                    referenceBitSet.set(i, true);
                    Assertions.assertThat(bitSet.nextClearBit(0)).describedAs(description).isEqualTo(referenceBitSet.nextClearBit(0));
                    Assertions.assertThat(bitSet.nextClearBit(i)).describedAs(description).isEqualTo(referenceBitSet.nextClearBit(i));
                } catch (RuntimeException exc) {
                    throw new IllegalStateException(description, exc);
                }
            }
        }
    }

    class TestAllocator implements Allocator {
        private final int blockSize;
        private long counter;

        TestAllocator(int blockSize) {
            this.blockSize = blockSize;
            counter = 0;
        }

        @Override
        public long allocate() {
            // should never allocate zero block, because it is not supported by ChunkList
            return counter += blockSize;
        }

        @Override
        public void deallocate(long offset) {
            throw new UnsupportedOperationException();
        }

        @Override
        public int getBlockSize() {
            return blockSize;
        }
    }
}