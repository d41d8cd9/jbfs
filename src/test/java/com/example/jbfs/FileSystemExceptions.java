package com.example.jbfs;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.nio.file.Path;
import java.util.Collections;

import javax.annotation.ParametersAreNonnullByDefault;

import com.example.jbfs.exceptions.DirectoryIsNotEmpty;
import com.example.jbfs.exceptions.FileOrDirectoryExists;
import com.example.jbfs.exceptions.NameTooLong;
import com.example.jbfs.exceptions.NoSpaceLeft;
import com.example.jbfs.exceptions.NoSuchFileOrDirectory;
import com.example.jbfs.exceptions.NotADirectory;
import com.example.jbfs.exceptions.NotAFile;
import com.google.common.collect.ImmutableList;
import org.assertj.core.api.JUnitSoftAssertions;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

@ParametersAreNonnullByDefault
public class FileSystemExceptions {
    @ClassRule
    public static final TemporaryFolder temporaryFolder = new TemporaryFolder();
    private static final int BLOCK_SIZE = 32;
    private static final long NEW_FS_SIZE = BLOCK_SIZE * 4096;

    @Rule
    public JUnitSoftAssertions softly = new JUnitSoftAssertions();
    private FileSystem fileSystem;

    @Before
    public void setUp() throws IOException {
        Path backend = temporaryFolder.newFile().toPath();
        FileBackedFileSystem.format(backend, NEW_FS_SIZE, BLOCK_SIZE);
        this.fileSystem = FileBackedFileSystem.mount(backend);
    }

    @After
    public void tearDown() {
        try {
            if (fileSystem != null) {
                ((FileBackedFileSystem) fileSystem).close();
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Test
    public void directoryIsNotEmpty() throws InterruptedException {
        fileSystem.makeDirectory("/test");
        fileSystem.makeFile("/test/file");

        softly.assertThatCode(() -> fileSystem.deleteDirectory("/test"))
                .isInstanceOf(DirectoryIsNotEmpty.class);
    }

    @Test
    public void fileNameIsTooLong() throws InterruptedException {
        String notTooLong = String.join("", Collections.nCopies(255, "x"));
        fileSystem.makeDirectory("/subdir");

        softly.assertThatCode(() -> fileSystem.makeFile(notTooLong))
                .describedAs("Not too long, root directory")
                .doesNotThrowAnyException();
        softly.assertThatCode(() -> fileSystem.makeFile("/subdir/" + notTooLong))
                .describedAs("Not too long, subdirectory")
                .doesNotThrowAnyException();
        softly.assertThatCode(() -> fileSystem.makeFile(notTooLong + "x"))
                .describedAs("Too long, root directory")
                .isInstanceOf(NameTooLong.class);
        softly.assertThatCode(() -> fileSystem.makeFile("/subdir/" + notTooLong + "x"))
                .describedAs("Too long, subdirectory")
                .isInstanceOf(NameTooLong.class);
    }

    @Test
    public void fileOrDirectoryExists() {
        softly.assertThatCode(() -> fileSystem.makeDirectory("/subdir"))
                .describedAs("mkdir /subdir")
                .doesNotThrowAnyException();
        softly.assertThatCode(() -> fileSystem.makeFile("/subdir"))
                .describedAs("mkdir /subdir; touch /subdir")
                .isInstanceOf(FileOrDirectoryExists.class);
        softly.assertThatCode(() -> fileSystem.makeFile("/subdir/file"))
                .describedAs("mkdir /subdir; touch /subdir/file")
                .doesNotThrowAnyException();
        softly.assertThatCode(() -> fileSystem.makeDirectory("/subdir/file"))
                .describedAs("mkdir /subdir; touch /subdir/file; mkdir /subdir/file")
                .isInstanceOf(FileOrDirectoryExists.class);
    }

    @Test
    public void noSpaceLeftByCreateFile() {
        softly.assertThatCode(
                () -> {
                    for (int i = 0; i < 10000; ++i) {
                        fileSystem.makeFile("/file" + i);
                    }
                })
                .isInstanceOf(NoSpaceLeft.class);
    }

    @Test
    public void noSpaceLeftByWritingOneFile() throws InterruptedException, IOException {
        fileSystem.makeFile("/file");
        try (Writer writer = new OutputStreamWriter(fileSystem.openForWriting("/file"))) {
            softly.assertThatCode(
                    () -> {
                        for (int i = 0; i < 10000; ++i) {
                            writer.write("this is the long-long string\n");
                        }
                    })
                    .isInstanceOf(NoSpaceLeft.class);
        }
    }

    @Test
    public void noSuchFileOrDirectory() throws InterruptedException {
        softly.assertThatCode(() -> fileSystem.makeDirectory("/oops/oops2/dir"))
                .describedAs("mkdir /oops/oops2/dir")
                .isInstanceOf(NoSuchFileOrDirectory.class)
                .extracting("path")
                .isEqualTo(ImmutableList.of("/oops"));
        softly.assertThatCode(() -> fileSystem.makeFile("/oops/oops2/file"))
                .describedAs("touch /oops/oops2/dir")
                .isInstanceOf(NoSuchFileOrDirectory.class)
                .extracting("path")
                .isEqualTo(ImmutableList.of("/oops"));

        fileSystem.makeDirectory("/oops");
        softly.assertThatCode(() -> fileSystem.makeDirectory("/oops/oops2/dir"))
                .describedAs("mkdir /oops; mkdir /oops/oops2/dir")
                .isInstanceOf(NoSuchFileOrDirectory.class)
                .extracting("path")
                .isEqualTo(ImmutableList.of("/oops/oops2"));
        softly.assertThatCode(() -> fileSystem.makeFile("/oops/oops2/file"))
                .describedAs("mkdir /oops; touch /oops/oops2/dir")
                .isInstanceOf(NoSuchFileOrDirectory.class)
                .extracting("path")
                .isEqualTo(ImmutableList.of("/oops/oops2"));

        softly.assertThatCode(() -> fileSystem.openForReading("/ololo"))
                .describedAs("cat /ololo")
                .isInstanceOf(NoSuchFileOrDirectory.class);
        softly.assertThatCode(() -> fileSystem.openForWriting("/ololo"))
                .describedAs("cat > /ololo")
                .isInstanceOf(NoSuchFileOrDirectory.class);
        softly.assertThatCode(() -> fileSystem.deleteFile("/ololo"))
                .describedAs("rm /ololo")
                .isInstanceOf(NoSuchFileOrDirectory.class);
        softly.assertThatCode(() -> fileSystem.deleteDirectory("/ololo"))
                .describedAs("rmdir /ololo")
                .isInstanceOf(NoSuchFileOrDirectory.class);
        softly.assertThatCode(() -> fileSystem.listDirectory("/ololo"))
                .describedAs("ls /ololo")
                .isInstanceOf(NoSuchFileOrDirectory.class);
    }

    @Test
    public void notADirectory() throws InterruptedException {
        fileSystem.makeFile("/lol");

        softly.assertThatCode(() -> fileSystem.makeDirectory("/lol/dir"))
                .describedAs("touch /lol; mkdir /lol/dir")
                .isInstanceOf(NotADirectory.class);
        softly.assertThatCode(() -> fileSystem.makeFile("/lol/file"))
                .describedAs("touch /lol; touch /lol/file")
                .isInstanceOf(NotADirectory.class);
        softly.assertThatCode(() -> fileSystem.listDirectory("/lol"))
                .describedAs("touch /lol; ls /lol")
                .isInstanceOf(NotADirectory.class);
        softly.assertThatCode(() -> fileSystem.listDirectory("/lol/dir"))
                .describedAs("touch /lol; ls /lol/dir")
                .isInstanceOf(NotADirectory.class);
        softly.assertThatCode(() -> fileSystem.deleteDirectory("/lol/dir"))
                .describedAs("touch /lol; rmdir /lol/dir")
                .isInstanceOf(NotADirectory.class);
        softly.assertThatCode(() -> fileSystem.deleteDirectory("/lol"))
                .describedAs("touch /lol; rmdir /lol")
                .isInstanceOf(NotADirectory.class);
        softly.assertThatCode(() -> fileSystem.deleteFile("/lol/file"))
                .describedAs("touch /lol; rmdir /lol/file")
                .isInstanceOf(NotADirectory.class);
    }

    @Test
    public void notAFile() throws InterruptedException {
        fileSystem.makeDirectory("/lol");

        softly.assertThatCode(() -> fileSystem.openForReading("/lol"))
                .describedAs("mkdir /lol; cat /lol")
                .isInstanceOf(NotAFile.class);
        softly.assertThatCode(() -> fileSystem.openForWriting("/lol"))
                .describedAs("mkdir /lol; cat > /lol")
                .isInstanceOf(NotAFile.class);
        softly.assertThatCode(() -> fileSystem.deleteFile("/lol"))
                .describedAs("mkdir /lol; rm /lol")
                .isInstanceOf(NotAFile.class);
    }
}
