package com.example.jbfs;

import java.util.stream.Stream;

import javax.annotation.ParametersAreNonnullByDefault;

import org.assertj.core.api.JUnitSoftAssertions;
import org.junit.Rule;
import org.junit.Test;

@ParametersAreNonnullByDefault
public class CloseableListTest {
    @Rule
    public JUnitSoftAssertions softly = new JUnitSoftAssertions();

    @Test
    public void ok() {
        CloseableList<TestCloseable> target = new CloseableList<>();
        for (int i = 0; i < 10; ++i) {
            target.add(new Ok());
        }

        softly.assertThatCode(target::close).doesNotThrowAnyException();
        softly.assertThat(target.stream().filter(o -> !o.closed)).isEmpty();
    }

    @Test
    public void oneFails() {
        CloseableList<TestCloseable> target = new CloseableList<>();
        for (int i = 0; i < 10; ++i) {
            target.add(new Ok());
            if (i == 5) {
                target.add(new Fail());
            }
        }

        softly.assertThatCode(target::close)
                .isInstanceOf(IllegalStateException.class)
                .matches(e -> e.getSuppressed().length == 1, "has exactly one suppressed exception")
                .matches(e -> Stream.of(e.getSuppressed()).allMatch(s -> s instanceof TestException), "all exceptions are of expected class");
        softly.assertThat(target.stream().filter(o -> !o.closed)).isEmpty();
    }

    @Test
    public void halfFails() {
        CloseableList<TestCloseable> target = new CloseableList<>();
        for (int i = 0; i < 10; ++i) {
            target.add(i % 2 == 0 ? new Ok() : new Fail());
        }

        softly.assertThatCode(target::close)
                .isInstanceOf(IllegalStateException.class)
                .matches(e -> e.getSuppressed().length == target.size() / 2, "has exactly half of list suppressed exceptions")
                .matches(e -> Stream.of(e.getSuppressed()).allMatch(s -> s instanceof TestException), "all exceptions are of expected class");
        softly.assertThat(target.stream().filter(o -> !o.closed)).isEmpty();
    }

    @Test
    public void allFails() {
        CloseableList<TestCloseable> target = new CloseableList<>();
        for (int i = 0; i < 10; ++i) {
            target.add(new Fail());
        }

        softly.assertThatCode(target::close)
                .isInstanceOf(IllegalStateException.class)
                .matches(e -> e.getSuppressed().length == target.size(), "all suppressed exceptions tracked")
                .matches(e -> Stream.of(e.getSuppressed()).allMatch(s -> s instanceof TestException), "all exceptions are of expected class");
        softly.assertThat(target.stream().filter(o -> !o.closed)).isEmpty();
    }

    private static class TestException extends Exception {
    }

    private static abstract class TestCloseable implements AutoCloseable {
        boolean closed = false;

        @Override
        public void close() throws Exception {
            closed = true;
        }
    }

    private static class Ok extends TestCloseable {
    }

    private static class Fail extends TestCloseable {
        @Override
        public void close() throws Exception {
            super.close();
            throw new TestException();
        }
    }
}