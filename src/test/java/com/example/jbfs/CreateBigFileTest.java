package com.example.jbfs;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

/**
 * Test based on snippet from interview feedback.
 */
public class CreateBigFileTest {
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void test() throws IOException, InterruptedException {
        File bigfile = makeBigFile();
        File fsFile = temporaryFolder.newFile();
        int blockSize = 4096;
        FileBackedFileSystem.format(fsFile.toPath(), (1 << 30) + (1 << 29), blockSize);
        FileBackedFileSystem fs = FileBackedFileSystem.mount(fsFile.toPath());
        try {
            fs.makeFile(bigfile.getName());
            OutputStream os = fs.openForWriting(bigfile.getName());
            try {
                Files.copy(bigfile.toPath(), os);
            } finally {
                os.close();
            }
        } finally {
            fs.close();
        }
        fs = FileBackedFileSystem.mount(fsFile.toPath());
        try {
            InputStream is = fs.openForReading(bigfile.getName());
            try {
                Files.copy(is, temporaryFolder.newFolder().toPath().resolve("dstFile"));
            } finally {
                is.close();
            }
        } finally {
            fs.close();
        }
    }

    private File makeBigFile() throws IOException {
        byte[] chunk = new byte[65536];
        ThreadLocalRandom.current().nextBytes(chunk);
        int chunkCount = 800 * 1024 * 1024 / chunk.length;  // 800 MiB file
        File result = temporaryFolder.newFile();
        try (OutputStream stream = new BufferedOutputStream(new FileOutputStream(result))) {
            while (chunkCount-- > 0) {
                stream.write(chunk);
            }
        }
        return result;
    }
}
